const db = require('./baza.js');
db.sequelize.sync({force:true}).then(function(){

    inicializacija().then(function(){
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
        process.exit();
    });
})

function inicializacija(){
    var studentiListaPromisea = [];
    var grupeListaPromisea = [];
    var vjezbeListaPromisea = [];
    var zadaciListaPromisea = [];

    return new Promise(function(resolve,reject){
        
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak1V1'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak2V1'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak3V1'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak1V2'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak2V2'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak3V2'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak1V3'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak2V3'}));

        
        studentiListaPromisea.push(db.student.create({ime:'Muharem',prezime:'Kapo',index:'18631'}));
        studentiListaPromisea.push(db.student.create({ime:'Muhamed',prezime:'Kapo',index:'17632'}));
        studentiListaPromisea.push(db.student.create({ime:'Sulejman',prezime:'Kapo',index:'19631'}));

        Promise.all(zadaciListaPromisea).then(resultsZadaci => {
            
            let zadatak1 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak1V1"})[0];
            let zadatak2 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak2V1"})[0];
            let zadatak3 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak3V1"})[0];
            let zadatak4 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak1V2"})[0];
            let zadatak5 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak2V2"})[0];
            let zadatak6 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak3V2"})[0];
            let zadatak7 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak1V3"})[0];
            let zadatak8 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak2V3"})[0];


            vjezbeListaPromisea.push(db.vjezba.create({naziv:'Vjezba1'}).then(vjezba => {
                vjezba.setZadaci([zadatak1,zadatak2,zadatak3]);
                return new Promise(function(resolve,reject){resolve(vjezba);});
            }));
            vjezbeListaPromisea.push(db.vjezba.create({naziv:'Vjezba2'}).then(vjezba => {
                vjezba.setZadaci([zadatak4,zadatak5,zadatak6]);
                return new Promise(function(resolve,reject){resolve(vjezba);});
            }));
            vjezbeListaPromisea.push(db.vjezba.create({naziv:'Vjezba3'}).then(vjezba => {
                vjezba.setZadaci([zadatak7,zadatak8]);
                return new Promise(function(resolve,reject){resolve(vjezba);});
            }));

            Promise.all(studentiListaPromisea.concat(vjezbeListaPromisea)).then(function(results){

                var student1 = results.filter(function(student){return student.ime == 'Muharem'})[0];
                var student2 = results.filter(function(student){return student.ime == 'Muhamed'})[0];
                var student3 = results.filter(function(student){return student.ime == 'Sulejman'})[0];
                
                var vjezba1 = results.filter(function(vjezba){return vjezba.naziv == 'Vjezba1'})[0];
                var vjezba2 = results.filter(function(vjezba){return vjezba.naziv == 'Vjezba2'})[0];
                var vjezba3 = results.filter(function(vjezba){return vjezba.naziv == 'Vjezba3'})[0];
    
                grupeListaPromisea.push(db.grupa.create({naziv:'Grupa1'}).then(grupa=>{
                    grupa.setStudenti([student1,student2]);
                    grupa.setVjezbe([vjezba1,vjezba2]);
                }));
                grupeListaPromisea.push(db.grupa.create({naziv:'Grupa2'}).then(grupa=>{
                    grupa.setStudenti([student3]);
                    grupa.setVjezbe([vjezba3]);
                }));
    
                Promise.all(grupeListaPromisea).then(function(grupe){
                    console.log("Proslo sve");
                }).catch(function(err){
                    console.log("Problem kod grupa");
                })
    
            }).catch(function(err){
                console.log("Problem kod studenata i vjezbi");
            })

        }).catch(function(err){
            console.log("Problem kod zadataka");
            console.log(err);
        })
            
        
    })
}