const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
//const db = require('./baza.js');

let port;
let db;
if (process.env.NODE_ENV === 'test') {
    db = require('./testBaza.js');
    port = 3001;
}
else {
    db = require('./baza.js');
    port = 3000
}


var rawBodySaver = function (req, res, buf, encoding) {
    if (buf && buf.length) {
      req.rawBody = buf.toString(encoding || 'utf8');
    }
  }
  
  app.use(bodyParser.json({ verify: rawBodySaver }));
  app.use(bodyParser.urlencoded({ verify: rawBodySaver, extended: true }));
  app.use(bodyParser.raw({ verify: rawBodySaver, type: '*/*' }));
app.use(express.static('public/html'));
app.use(express.static('public'));


app.get('/vjezbe',function(req,res){
        
    db.vjezba.findAll().then(function(results){
        let brojVjezbi = results.length;
        let nizPromiseZadataka = [];
        
        for(let i=0; i<results.length; i++){
            nizPromiseZadataka.push(results[i].getZadaci().then(resultsZadaci => {
                return new Promise(function(resolve,reject){
                    resolve(resultsZadaci.length);
                })
            }))
        }
        Promise.all(nizPromiseZadataka).then(function(results){
            
            res.send(JSON.stringify({brojVjezbi:brojVjezbi, brojZadataka:results}));
        },function(){
            console.log("Nije uspjelo dobavljanje zadataka");
        })

    })          
})

app.post('/vjezbe',function(req,res){
    let tijeloZahtjeva = req.body;
    let pogresanParametar = "";

    let brojVjezbi = tijeloZahtjeva.brojVjezbi;
    let brojZadataka = tijeloZahtjeva.brojZadataka;

    if(brojVjezbi!=brojZadataka.length){
        res.send(JSON.stringify({status:"error",data:"Pogrešna velicina parametra brojZadataka"}));
    }
    else{
        if(brojVjezbi<1 || brojVjezbi>15){
            pogresanParametar = pogresanParametar + "brojVjezbi";
        }
        
        for (let i=0; i<brojZadataka.length; i++){
            if(brojZadataka[i]<0 || brojZadataka[i]>10){
                if(pogresanParametar!=""){
                    pogresanParametar = pogresanParametar + ",z" + i;
                }
                else{
                    pogresanParametar = pogresanParametar + "z" + i;
                }
            }
        }
        

        if(pogresanParametar!=""){
            res.send(JSON.stringify({status:"error",data:"Pogrešan parametar " + pogresanParametar}));
        }
        else{

            db.zadatak.destroy({
                where : {}
              }).then(()=>{
                db.vjezba.destroy({
                    where : {}
                  }).then(()=>{
                
                    let vjezbaListaPromiesea = [];

                    for(let i=0; i<brojVjezbi; i++){
                        vjezbaListaPromiesea.push(dodajVjezbeIZadatake(brojZadataka,i));
                    }

                    Promise.all(vjezbaListaPromiesea).then(results => {
                        res.send(JSON.stringify({brojVjezbi:brojVjezbi, brojZadataka:brojZadataka}));
                    })
                })
            })
            
        }
    }   
})

app.post('/student',function(req,res){
    let tijeloZahtjeva = req.body;
    db.student.findOrCreate({
        where:{index:tijeloZahtjeva.index},
        defaults:{
            ime:tijeloZahtjeva.ime,
            prezime:tijeloZahtjeva.prezime
        }    
    }).then((resultStudent) => {
        
        if(!resultStudent[1]){
            res.send({status:"Student sa indexom " + tijeloZahtjeva.index + " već postoji!"});
        }
        else{
            db.grupa.findOrCreate({
                where:{naziv:tijeloZahtjeva.grupa}
            }).then((resultGrupa) => {
                
                promiseGrupa = resultGrupa[0].addStudenti(resultStudent[0]).then(()=>{
                    res.send({status:"Kreiran student!"});
                }).catch(function(err){
                    console.log(err);
                    res.send({status:"Nije uspjesno kreiran student\n"});
                });
            })   
        }
    })
})

app.put('/student/:index',function(req,res){
    let tijeloZahtjeva = req.body;
    let parametri = req.params
    db.student.findOne({
        where: {index: parametri.index}
    }).then(resultStudent => {
        if(resultStudent==undefined){
            res.send({status:"Student sa indexom " + parametri.index + " ne postoji!"});
        }
        else{
            db.grupa.findOrCreate({
                where: {naziv:tijeloZahtjeva.grupa}
            }).then(resultGrupa => {
                resultGrupa[0].addStudenti(resultStudent).then(()=>{
                    res.send({status:"Promijenjena grupa studentu "  + parametri.index });
                }); 
            }).catch(function(err){
                
                res.send("Nije uspjelo dodavanje grupe studentu");
            })
        }
    }).catch(function(err){
        res.send("Nije uspjelo trazenje studenta");
    })
})

app.post('/batch/student', function(req,res){
    let tijeloZahtjeva = req.rawBody;
    let studenti = tijeloZahtjeva.split(/\r*\n/);

    let dodavanjeGrupaPromise = [];
    let grupeZaDodati = [];
    for(let i=0; i<studenti.length; i++){
        noviStudent = studenti[i].split(",");
        grupeZaDodati.push(noviStudent[3]);
    }
    grupeZaDodati = grupeZaDodati.filter(function(item,position){
        return grupeZaDodati.indexOf(item)==position;
    })
    
    for(let i=0; i<grupeZaDodati.length; i++){
        dodavanjeGrupaPromise.push(db.grupa.findOrCreate(
            {
                where : {naziv : grupeZaDodati[i]}
            }
        ).then(resultGrupa => {}))
    }

    Promise.all(dodavanjeGrupaPromise).then(()=>{

            
        let dodavanjeStudenataPromise = [];
        for(let i=0; i<studenti.length; i++){
            noviStudent = studenti[i].split(",");
            dodavanjeStudenataPromise.push(
                dodavanjeStudenataCSV (noviStudent)
            )
        }
        Promise.all(dodavanjeStudenataPromise).then((values)=>{
            
            let dodaniStudenti = values.
                filter(studentic => {return studentic[1] == true}).
                map(studentic => {return studentic[0].index;});

            let nedodaniStudenti = values.
                filter(studentic => {return studentic[1] == false}).
                map(studentic => {return studentic[0].index;});

            if(dodaniStudenti.length == studenti.length){
                res.send({status:"Dodano " + dodaniStudenti.length + " studenata!"});
            }
            else{
                res.send({status:"Dodano " + dodaniStudenti.length + " studenata, a studenti " + nedodaniStudenti.join(",") + " već postoje!"});
            }
        })
    })
})


app.listen(port);

function dodavanjeStudenataCSV (noviStudent){
    
    return new Promise(function(resolve,reject){
        db.student.findOrCreate(
            {
                where:{index:noviStudent[2]},
                defaults : 
                {
                    ime: noviStudent[0],
                    prezime: noviStudent[1]
                }
            }
        ).then(resultStudent => {
            resolve(resultStudent);
            db.grupa.findOne(
                {
                    where : {naziv: noviStudent[3]}
                }).then(resultGrupa => {
                resultGrupa.addStudenti(resultStudent[0]);
                return new Promise (function(resolve,reject){});
            })
        })
    
    })
    
}


var brojacZadataka = 1;
function dodajVjezbeIZadatake(brojZadataka, redniBroj){
    new Promise(function(resolve,reject){
        db.vjezba.create({id:redniBroj+1, naziv:"Vjezba"+(redniBroj+1)}).then(resultVjezba =>{
        
            let zadaciListaPromisea = [];
            for(let j=0; j<brojZadataka[redniBroj];j++){
                
                zadaciListaPromisea.push(db.zadatak.create({id:brojacZadataka,naziv:"Zadatak"+(j+1)+""+"V"+(redniBroj+1)}))
                brojacZadataka++;
                
            }
            Promise.all(zadaciListaPromisea).then(results => {
                resultVjezba.setZadaci(results);
            })
        })
    })
}

module.exports = app;