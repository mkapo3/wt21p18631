const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt21INDEX", "root", "password", {
   host: "127.0.0.1",
   dialect: "mysql"
});
// module.exports = sequelize;
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

db.student = require('./modeli/student.js')(sequelize);
db.grupa = require('./modeli/grupa.js')(sequelize);
db.vjezba = require('./modeli/vjezba.js')(sequelize);
db.zadatak = require('./modeli/zadatak.js')(sequelize);

db.grupa.hasMany(db.student,{as:'studenti'});
db.grupa.hasMany(db.vjezba,{as:'vjezbe'});
db.vjezba.hasMany(db.zadatak,{as:'zadaci'});

// db.vjezbaZadatak = db.vjezba.belongsToMany(db.zadatak,{as:'zadaci',through:'vjezba_zadatak',foreignKey:'vjezbaId'});
// db.zadatak.belongsToMany(db.vjezba,{as:'vjezbe',through:'vjezba_zadatak',foreignKey:'zadatakId'});

module.exports = db;