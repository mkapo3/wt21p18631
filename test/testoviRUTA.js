const request = require('supertest');
const assert = require('assert');
let app;
const db = require('../testBaza.js');


describe('Testovi StudentAjax',function(){
    before(function(done){
        process.env.NODE_ENV = 'test';
        app = require('../index.js');
        
        db.sequelize.sync({force:true}).then(function(){

            inicializacija().then(function(){
                console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
            });
            return done();
        })
    })

     describe('Testovi post /student', function(){
        it('Student postoji',function(done){
            let student = {
                ime : "Muharem",
                prezime : "Kapo",
                index : "18631",
                grupa : "Grupa1"
            }

            request(app)
                .post('/student')
                .send(student)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, 'Student sa indexom 18631 već postoji!',""); 
                   return done();
                }).catch(err => {
                    done(err);
                })
        })
        it('Student ne postoji grupa postoji',function(done){
            let student = {
                ime : "NekoTamo",
                prezime : "Nekic",
                index : "16664",
                grupa : "Grupa1"
            }

            request(app)
                .post('/student')
                .send(student)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, 'Kreiran student!',""); 
                   return done();
                }).catch(err => {
                    done(err);
                })
         })
        it('Student ne postoji grupa ne postoji',function(done){
            let student = {
                ime : "Studo",
                prezime : "Studentic",
                index : "11107",
                grupa : "Grupa10"
            }

            request(app)
                .post('/student')
                .send(student)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, 'Kreiran student!',""); 
                   return done();
                }).catch(err => {
                    done(err);
                })
        })
     })

    describe('Testovi put /student/index',function(){
        it('Student postoji grupa postoji',function(done){
            let student = {
                grupa : "Grupa2"
            }
            let index = "18631"
            request(app)
                .put('/student/'+index)
                .send(student)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, "Promijenjena grupa studentu "  + index,""); 
                   return done();
                }).catch(err => {
                    done(err);
                })
        })
        it('Student ne postoji',function(done){
            let student = {
                grupa : "Grupa2"
            }
            let index = "22222"
            request(app)
                .put('/student/'+index)
                .send(student)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, "Student sa indexom " + index + " ne postoji!",""); 
                   return done();
                }).catch(err => {
                    done(err);
                })
            
        })
        it('Student postoji grupa ne postoji',function(done){
            let student = {
                grupa : "Grupa12"
            }
            let index = "18631"
            request(app)
                .put('/student/'+index)
                .send(student)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, "Promijenjena grupa studentu "  + index,""); 
                   return done();
                }).catch(err => {
                    done(err);
                })
        })
    })

    describe('Testvoi post /batch/student',function(){
        it('Svi studenti dodani',function(done){
            let csv = "Mujo,Mujic,11109,Grupa1\n" +
                "Safet,Safetic,11110,Grupa1\n" +
                "Suljo,Suljic,11111,Grupa2";
            
            request(app)
                .post('/batch/student')
                .send(csv)
                .set('Accept','application/raw')
                .expect('Content-Type',/json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, "Dodano " + 3 + " studenata!"); 
                   return done();
                }).catch(err => {
                    done(err);
                })

        })
        it('Dodano M, a nije N - standardni slucaj #1',function(done){
            let csv = "Zajko,Zajcic,11112,Grupa1\n" +
                "Muharem,Muharemic,11113,Grupa1\n" +
                "Muharem,Kapo,18631,Grupa2";
            
            request(app)
                .post('/batch/student')
                .send(csv)
                .set('Accept','application/raw')
                .expect('Content-Type',/json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, "Dodano " + 2 + " studenata, a studenti " + 18631 + " već postoje!", "Dva studenta dodana, a 18631 nije."); 
                   return done();
                }).catch(err => {
                    done(err);
                })
        })
        // Za razliku od prethodnog ovdje ima vise studenata koji se
        // ne mogu dodati, tako da se provjerava redoslijed
        it('Dodano M, a nije N - standardni slucaj #2',function(done){
            let csv = "Mujo,Mujicic,11115,Grupa1\n" +
                "Safet,Safeticic,11116,Grupa1\n" +
                "Muharem,Kapo,18631,Grupa2\n" +
                "Muhamed,Kapo,17632,Grupa2";
            
            
            request(app)
                .post('/batch/student')
                .send(csv)
                .set('Accept','application/raw')
                .expect('Content-Type',/json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, "Dodano " + 2 + " studenata, a studenti 18631,17632 već postoje!", "Dva studenta dodana, a 18631,17632 nije."); 
                   return done();
                }).catch(err => {
                    done(err);
                })
        })
        //Isti kao prethodni samo zamijenjen redoslijed neubacenih studenata
        it('Dodano M, a nije N - standardni slucaj #3',function(done){
            let csv = "Mujo,Mujicic,11117,Grupa1\n" +
                "Safet,Safeticic,11118,Grupa1\n" +
                "Muhamed,Kapo,17632,Grupa2\n" +
                "Muharem,Kapo,18631,Grupa2";
            
            
            request(app)
                .post('/batch/student')
                .send(csv)
                .set('Accept','application/raw')
                .expect('Content-Type',/json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, "Dodano " + 2 + " studenata, a studenti 17632,18631 već postoje!", "Dva studenta dodana, a 17632,18631 nije."); 
                   return done();
                }).catch(err => {
                    done(err);
                })
        })
        it('Dodano M, a nije N - isti indeksi #1',function(done){
            let csv = "Mujo,Mujicic,11119,Grupa1\n" +
                "Safet,Safeticic,11120,Grupa1\n" +
                "NekoTamoPrvi,Kapo,33333,Grupa2\n" +
                "NekoTamoDrugi,Kapo,33333,Grupa3\n" +
                "NekoTamoTreci,Kapo,33333,Grupa1";
            
            
            request(app)
                .post('/batch/student')
                .send(csv)
                .set('Accept','application/raw')
                .expect('Content-Type',/json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, "Dodano " + 3 + " studenata, a studenti 33333,33333 već postoje!", "Tri studenta dodana, a 33333,33333 nije."); 
                   return done();
                }).catch(err => {
                    done(err);
                })
        })
        //Ovdje se testira da li je raspored nedodanih dobar - 33334,17632,33334
        it('Dodano M, a nije N - isti indeksi #2',function(done){
            let csv = "Mujo,Mujicic,11121,Grupa1\n" +
                "Safet,Safeticic,11122,Grupa1\n" +
                "NekoTamoPrvi,Kapo,33334,Grupa2\n" +
                "NekoTamoDrugi,Kapo,33334,Grupa3\n" +
                "OvamoNeko,Kapo,17632,Grupa3\n" +
                "NekoTamoTreci,Kapo,33334,Grupa1";

                request(app)
                .post('/batch/student')
                .send(csv)
                .set('Accept','application/raw')
                .expect('Content-Type',/json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, "Dodano " + 3 + " studenata, a studenti 33334,17632,33334 već postoje!", "Tri studenta dodana, a 33334,17632,33334 nije."); 
                   return done();
                }).catch(err => {
                    done(err);
                })
        })
        it('Nije nijedan dodan',function(done){
            let csv = "Mujo,Mujicic,18631,Grupa1\n" +
                "Safet,Safeticic,17632,Grupa1\n" +
                "NekoTamoPrvi,Kapo,19631,Grupa2";

                request(app)
                .post('/batch/student')
                .send(csv)
                .set('Accept','application/raw')
                .expect('Content-Type',/json/)
                .expect(200)
                .then(response =>{
                   console.log(response.body.status)
                   assert.equal(response.body.status, "Dodano " + 0 + " studenata, a studenti 18631,17632,19631 već postoje!", "Tri studenta dodana, a 18631,17632,19631 nije."); 
                   return done();
                }).catch(err => {
                    done(err);
                })
        })
        
    })
 })


function inicializacija(){
    var studentiListaPromisea = [];
    var grupeListaPromisea = [];
    var vjezbeListaPromisea = [];
    var zadaciListaPromisea = [];

    return new Promise(function(resolve,reject){
        
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak1V1'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak2V1'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak3V1'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak1V2'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak2V2'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak3V2'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak1V3'}));
        zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak2V3'}));

        
        studentiListaPromisea.push(db.student.create({ime:'Muharem',prezime:'Kapo',index:'18631'}));
        studentiListaPromisea.push(db.student.create({ime:'Muhamed',prezime:'Kapo',index:'17632'}));
        studentiListaPromisea.push(db.student.create({ime:'Sulejman',prezime:'Kapo',index:'19631'}));

        Promise.all(zadaciListaPromisea).then(resultsZadaci => {
            
            let zadatak1 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak1V1"})[0];
            let zadatak2 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak2V1"})[0];
            let zadatak3 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak3V1"})[0];
            let zadatak4 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak1V2"})[0];
            let zadatak5 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak2V2"})[0];
            let zadatak6 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak3V2"})[0];
            let zadatak7 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak1V3"})[0];
            let zadatak8 = resultsZadaci.filter(function(zadatak){return zadatak.naziv == "Zadatak2V3"})[0];


            vjezbeListaPromisea.push(db.vjezba.create({naziv:'Vjezba1'}).then(vjezba => {
                vjezba.setZadaci([zadatak1,zadatak2,zadatak3]);
                return new Promise(function(resolve,reject){resolve(vjezba);});
            }));
            vjezbeListaPromisea.push(db.vjezba.create({naziv:'Vjezba2'}).then(vjezba => {
                vjezba.setZadaci([zadatak4,zadatak5,zadatak6]);
                return new Promise(function(resolve,reject){resolve(vjezba);});
            }));
            vjezbeListaPromisea.push(db.vjezba.create({naziv:'Vjezba3'}).then(vjezba => {
                vjezba.setZadaci([zadatak7,zadatak8]);
                return new Promise(function(resolve,reject){resolve(vjezba);});
            }));

            Promise.all(studentiListaPromisea.concat(vjezbeListaPromisea)).then(function(results){

                var student1 = results.filter(function(student){return student.ime == 'Muharem'})[0];
                var student2 = results.filter(function(student){return student.ime == 'Muhamed'})[0];
                var student3 = results.filter(function(student){return student.ime == 'Sulejman'})[0];
                
                var vjezba1 = results.filter(function(vjezba){return vjezba.naziv == 'Vjezba1'})[0];
                var vjezba2 = results.filter(function(vjezba){return vjezba.naziv == 'Vjezba2'})[0];
                var vjezba3 = results.filter(function(vjezba){return vjezba.naziv == 'Vjezba3'})[0];
    
                grupeListaPromisea.push(db.grupa.create({naziv:'Grupa1'}).then(grupa=>{
                    grupa.setStudenti([student1,student2]);
                    grupa.setVjezbe([vjezba1,vjezba2]);
                }));
                grupeListaPromisea.push(db.grupa.create({naziv:'Grupa2'}).then(grupa=>{
                    grupa.setStudenti([student3]);
                    grupa.setVjezbe([vjezba3]);
                }));
    
                Promise.all(grupeListaPromisea).then(function(grupe){
                    console.log("Proslo sve");
                }).catch(function(err){
                    console.log("Problem kod grupa");
                })
    
            }).catch(function(err){
                console.log("Problem kod studenata i vjezbi");
            })

        }).catch(function(err){
            console.log("Problem kod zadataka");
            console.log(err);
        })
            
        
    })
}