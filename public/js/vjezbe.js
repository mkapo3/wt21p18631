var vjezbeObjekat;
function ispisi(error,data){
    if(!error){
        console.log(data);
        vjezbeObjekat = data;
        let divDOMelement = document.getElementById("odabirVjezbe");
        VjezbeAjax.iscrtajVjezbe(divDOMelement, JSON.parse(vjezbeObjekat));
    }
    else{
        console.log(error);
    }
}

window.onload = function(){
    VjezbeAjax.dohvatiPodatke(ispisi);
}

function iscrtajZadatke(vjezbaDOMelement, brojZadataka){
    
    if(vjezbaDOMelement.getElementsByClassName("zadaci").length == 0){
        VjezbeAjax.iscrtajZadatke(vjezbaDOMelement, brojZadataka);
    }

    if(!vjezbaDOMelement.classList.contains("vidljivaVjezba")){
        sveVidljiveVjezbe = document.getElementsByClassName("vidljivaVjezba");
        if(sveVidljiveVjezbe.length != 0){
            
            vidljivaVjezba = sveVidljiveVjezbe[0];
            vidljivaVjezba.querySelector(".zadaci").classList.add("zadaciInvisible");
            vidljivaVjezba.classList.remove("vidljivaVjezba");
            console.log(vidljivaVjezba);

        }
        vjezbaDOMelement.querySelector(".zadaci").classList.remove("zadaciInvisible");
        vjezbaDOMelement.classList.add("vidljivaVjezba");
    }
}

