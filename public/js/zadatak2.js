let assert = chai.assert;
var json_string_test = `{
    "stats": {
      "suites": 5,
      "tests": 19,
      "passes": 9,
      "pending": 0,
      "failures": 10,
      "start": "2021-11-17T18:40:46.180Z",
      "end": "2021-11-17T18:40:46.189Z",
      "duration": 9
    },
    "tests": [
      {
        "title": "Tabela nema redova #1",
        "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj redova 4 #1",
        "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj vidljivih celija 6 #1",
        "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj nevidljivih celija 10 #1",
        "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Vide se samo celije ispod dijagonale #1",
        "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Tabela nema redova #2",
        "fullTitle": "Tabela crtaj2() Tabela nema redova #2",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Tabela ima 1 red: expected 0 to equal 1",
          "showDiff": true,
          "actual": "0",
          "expected": "1",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela ima 1 red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:70:16)"
        }
      },
      {
        "title": "Broj redova 4 #2",
        "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj redova treba biti 3: expected 4 to equal 3",
          "showDiff": true,
          "actual": "4",
          "expected": "3",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak2.js:78:16)"
        }
      },
      {
        "title": "Broj vidljivih celija 6 #2",
        "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj vidljivih celija 10: expected 6 to equal 10",
          "showDiff": true,
          "actual": "6",
          "expected": "10",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:92:16)"
        }
      },
      {
        "title": "Broj nevidljivih celija 10 #2",
        "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Vide se samo celije ispod dijagonale #2",
        "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
          "showDiff": true,
          "actual": "true",
          "expected": "false",
          "operator": "strictEqual",
          "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak2.js:112:16)"
        }
      },
      {
        "title": "Tabela nema redova #3",
        "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Tabela ima red: expected 0 to equal 1",
          "showDiff": true,
          "actual": "0",
          "expected": "1",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:131:16)"
        }
      },
      {
        "title": "Broj redova 4 #3",
        "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj redova treba biti 2: expected 4 to equal 2",
          "showDiff": true,
          "actual": "4",
          "expected": "2",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj redova treba biti 2: expected 4 to equal 2\n    at Context.<anonymous> (zadatak2.js:139:16)"
        }
      },
      {
        "title": "Broj vidljivih celija 6 #3",
        "fullTitle": "Tabela crtaj3() Broj vidljivih celija 6 #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj vidljivih celija 10: expected 6 to equal 10",
          "showDiff": true,
          "actual": "6",
          "expected": "10",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:153:16)"
        }
      },
      {
        "title": "Broj nevidljivih celija 10 #3",
        "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
          "showDiff": true,
          "actual": "10",
          "expected": "6",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak2.js:166:16)"
        }
      },
      {
        "title": "Tabela nema redova #4",
        "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj redova 4 #4",
        "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj vidljivih celija 6 #4",
        "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:210:17)",
          "message": "brojCelijaGRESKA is not defined"
        }
      },
      {
        "title": "Broj nevidljivih celija 10 #4",
        "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:224:17)",
          "message": "brojCelijaGRESKA is not defined"
        }
      },
      {
        "title": "Vide se samo celije ispod dijagonale #4",
        "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      }
    ],
    "pending": [],
    "failures": [
      {
        "title": "Tabela nema redova #2",
        "fullTitle": "Tabela crtaj2() Tabela nema redova #2",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Tabela ima 1 red: expected 0 to equal 1",
          "showDiff": true,
          "actual": "0",
          "expected": "1",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela ima 1 red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:70:16)"
        }
      },
      {
        "title": "Broj redova 4 #2",
        "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj redova treba biti 3: expected 4 to equal 3",
          "showDiff": true,
          "actual": "4",
          "expected": "3",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak2.js:78:16)"
        }
      },
      {
        "title": "Broj vidljivih celija 6 #2",
        "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj vidljivih celija 10: expected 6 to equal 10",
          "showDiff": true,
          "actual": "6",
          "expected": "10",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:92:16)"
        }
      },
      {
        "title": "Vide se samo celije ispod dijagonale #2",
        "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
          "showDiff": true,
          "actual": "true",
          "expected": "false",
          "operator": "strictEqual",
          "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak2.js:112:16)"
        }
      },
      {
        "title": "Tabela nema redova #3",
        "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Tabela ima red: expected 0 to equal 1",
          "showDiff": true,
          "actual": "0",
          "expected": "1",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:131:16)"
        }
      },
      {
        "title": "Broj redova 4 #3",
        "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj redova treba biti 2: expected 4 to equal 2",
          "showDiff": true,
          "actual": "4",
          "expected": "2",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj redova treba biti 2: expected 4 to equal 2\n    at Context.<anonymous> (zadatak2.js:139:16)"
        }
      },
      {
        "title": "Broj vidljivih celija 6 #3",
        "fullTitle": "Tabela crtaj3() Broj vidljivih celija 6 #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj vidljivih celija 10: expected 6 to equal 10",
          "showDiff": true,
          "actual": "6",
          "expected": "10",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:153:16)"
        }
      },
      {
        "title": "Broj nevidljivih celija 10 #3",
        "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
          "showDiff": true,
          "actual": "10",
          "expected": "6",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak2.js:166:16)"
        }
      },
      {
        "title": "Broj vidljivih celija 6 #4",
        "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:210:17)",
          "message": "brojCelijaGRESKA is not defined"
        }
      },
      {
        "title": "Broj nevidljivih celija 10 #4",
        "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:224:17)",
          "message": "brojCelijaGRESKA is not defined"
        }
      }
    ],
    "passes": [
      {
        "title": "Tabela nema redova #1",
        "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj redova 4 #1",
        "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj vidljivih celija 6 #1",
        "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj nevidljivih celija 10 #1",
        "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Vide se samo celije ispod dijagonale #1",
        "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj nevidljivih celija 10 #2",
        "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Tabela nema redova #4",
        "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj redova 4 #4",
        "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Vide se samo celije ispod dijagonale #4",
        "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      }
    ]
  }
    `
var json_string_test_2 = `{
        "stats": {
          "suites": 5,
          "tests": 18,
          "passes": 10,
          "pending": 0,
          "failures": 8,
          "start": "2021-11-17T18:43:17.331Z",
          "end": "2021-11-17T18:43:17.340Z",
          "duration": 9
        },
        "tests": [
          {
            "title": "Tabela nema redova #1",
            "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj redova 4 #1",
            "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj vidljivih celija 6 #1",
            "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj nevidljivih celija 10 #1",
            "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Vide se samo celije ispod dijagonale #1",
            "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj redova 4 #2",
            "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Broj redova treba biti 3: expected 4 to equal 3",
              "showDiff": true,
              "actual": "4",
              "expected": "3",
              "operator": "strictEqual",
              "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak3pomocni.js:78:16)"
            }
          },
          {
            "title": "Broj vidljivih celija 6 #2",
            "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Broj vidljivih celija 10: expected 6 to equal 10",
              "showDiff": true,
              "actual": "6",
              "expected": "10",
              "operator": "strictEqual",
              "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak3pomocni.js:92:16)"
            }
          },
          {
            "title": "Broj nevidljivih celija 10 #2",
            "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Vide se samo celije ispod dijagonale #2",
            "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
              "showDiff": true,
              "actual": "true",
              "expected": "false",
              "operator": "strictEqual",
              "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:112:16)"
            }
          },
          {
            "title": "Tabela nema redova #3",
            "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Tabela ima red: expected 0 to equal 1",
              "showDiff": true,
              "actual": "0",
              "expected": "1",
              "operator": "strictEqual",
              "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak3pomocni.js:131:16)"
            }
          },
          {
            "title": "Broj redova 4 #3",
            "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj nevidljivih celija 10 #3",
            "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
              "showDiff": true,
              "actual": "10",
              "expected": "6",
              "operator": "strictEqual",
              "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak3pomocni.js:166:16)"
            }
          },
          {
            "title": "Vide se samo celije ispod dijagonale #3",
            "fullTitle": "Tabela crtaj3() Vide se samo celije ispod dijagonale #3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
              "showDiff": true,
              "actual": "true",
              "expected": "false",
              "operator": "strictEqual",
              "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:173:16)"
            }
          },
          {
            "title": "Tabela nema redova #4",
            "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj redova 4 #4",
            "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj vidljivih celija 6 #4",
            "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:210:17)",
              "message": "brojCelijaGRESKA is not defined"
            }
          },
          {
            "title": "Broj nevidljivih celija 10 #4",
            "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:224:17)",
              "message": "brojCelijaGRESKA is not defined"
            }
          },
          {
            "title": "Vide se samo celije ispod dijagonale #4",
            "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          }
        ],
        "pending": [],
        "failures": [
          {
            "title": "Broj redova 4 #2",
            "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Broj redova treba biti 3: expected 4 to equal 3",
              "showDiff": true,
              "actual": "4",
              "expected": "3",
              "operator": "strictEqual",
              "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak3pomocni.js:78:16)"
            }
          },
          {
            "title": "Broj vidljivih celija 6 #2",
            "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Broj vidljivih celija 10: expected 6 to equal 10",
              "showDiff": true,
              "actual": "6",
              "expected": "10",
              "operator": "strictEqual",
              "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak3pomocni.js:92:16)"
            }
          },
          {
            "title": "Vide se samo celije ispod dijagonale #2",
            "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
              "showDiff": true,
              "actual": "true",
              "expected": "false",
              "operator": "strictEqual",
              "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:112:16)"
            }
          },
          {
            "title": "Tabela nema redova #3",
            "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Tabela ima red: expected 0 to equal 1",
              "showDiff": true,
              "actual": "0",
              "expected": "1",
              "operator": "strictEqual",
              "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak3pomocni.js:131:16)"
            }
          },
          {
            "title": "Broj nevidljivih celija 10 #3",
            "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
              "showDiff": true,
              "actual": "10",
              "expected": "6",
              "operator": "strictEqual",
              "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak3pomocni.js:166:16)"
            }
          },
          {
            "title": "Vide se samo celije ispod dijagonale #3",
            "fullTitle": "Tabela crtaj3() Vide se samo celije ispod dijagonale #3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
              "showDiff": true,
              "actual": "true",
              "expected": "false",
              "operator": "strictEqual",
              "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:173:16)"
            }
          },
          {
            "title": "Broj vidljivih celija 6 #4",
            "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:210:17)",
              "message": "brojCelijaGRESKA is not defined"
            }
          },
          {
            "title": "Broj nevidljivih celija 10 #4",
            "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:224:17)",
              "message": "brojCelijaGRESKA is not defined"
            }
          }
        ],
        "passes": [
          {
            "title": "Tabela nema redova #1",
            "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj redova 4 #1",
            "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj vidljivih celija 6 #1",
            "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj nevidljivih celija 10 #1",
            "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Vide se samo celije ispod dijagonale #1",
            "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj nevidljivih celija 10 #2",
            "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj redova 4 #3",
            "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Tabela nema redova #4",
            "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Broj redova 4 #4",
            "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "Vide se samo celije ispod dijagonale #4",
            "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          }
        ]
      }` 
var json_string_test_3 = `{
    "stats": {
      "suites": 2,
      "tests": 2,
      "passes": 0,
      "pending": 0,
      "failures": 2,
      "start": "2021-11-18T15:10:20.391Z",
      "end": "2021-11-18T15:10:20.396Z",
      "duration": 5
    },
    "tests": [
      {
        "title": "Tabela nema redova #1",
        "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Tabela nema redova: expected 0 to equal 1",
          "showDiff": true,
          "actual": "0",
          "expected": "1",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela nema redova: expected 0 to equal 1\n    at Context.<anonymous> (zadatak3pomocni.js:9:16)"
        }
      },
      {
        "title": "Broj redova 4 #1",
        "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Broj redova treba biti 4: expected 4 to equal 3",
          "showDiff": true,
          "actual": "4",
          "expected": "3",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj redova treba biti 4: expected 4 to equal 3\n    at Context.<anonymous> (zadatak3pomocni.js:17:16)"
        }
      }
    ],
    "pending": [],
    "failures": [
      {
        "title": "Tabela nema redova #1",
        "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Tabela nema redova: expected 0 to equal 1",
          "showDiff": true,
          "actual": "0",
          "expected": "1",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela nema redova: expected 0 to equal 1\n    at Context.<anonymous> (zadatak3pomocni.js:9:16)"
        }
      },
      {
        "title": "Broj redova 4 #1",
        "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Broj redova treba biti 4: expected 4 to equal 3",
          "showDiff": true,
          "actual": "4",
          "expected": "3",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj redova treba biti 4: expected 4 to equal 3\n    at Context.<anonymous> (zadatak3pomocni.js:17:16)"
        }
      }
    ],
    "passes": []
  }`
var json_string_test_4 = `{
    "stats": {
      "suites": 2,
      "tests": 2,
      "passes": 2,
      "pending": 0,
      "failures": 0,
      "start": "2021-11-18T15:19:50.333Z",
      "end": "2021-11-18T15:19:50.337Z",
      "duration": 4
    },
    "tests": [
      {
        "title": "Tabela nema redova #1",
        "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj redova 4 #1",
        "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      }
    ],
    "pending": [],
    "failures": [],
    "passes": [
      {
        "title": "Tabela nema redova #1",
        "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj redova 4 #1",
        "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      }
    ]
  }`
var json_string_test_5 = `{
    "stats": {
      "suites": 2,
      "tests": 1,
      "passes": 0,
      "pending": 0,
      "failures": 1,
      "start": "2021-11-18T15:25:13.428Z",
      "end": "2021-11-18T15:25:13.433Z",
      "duration": 5
    },
    "tests": [
      {
        "title": "Tabela nema redova #1",
        "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Tabela nema redova: expected 0 to equal 3",
          "showDiff": true,
          "actual": "0",
          "expected": "3",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (zadatak3pomocni.js:9:16)"
        }
      }
    ],
    "pending": [],
    "failures": [
      {
        "title": "Tabela nema redova #1",
        "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Tabela nema redova: expected 0 to equal 3",
          "showDiff": true,
          "actual": "0",
          "expected": "3",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (zadatak3pomocni.js:9:16)"
        }
      }
    ],
    "passes": []
  }`
  //NAPRAVLJENE GRESKE U JSON-u
var nevaljasti_json_string_test = `{
    "stats": {
      "suites": 5,
      "tests": 19,
      "passes": 9,
      "pending": 0
      "failures": 10,
      "start": "2021-11-17T18:40:46.180Z",
      "end": "2021-11-17T18:40:46.189Z",
      "duration": 9
    
    "tests": 
      {
        "title": "Tabela nema redova #1",
        "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      
      {
        "title": "Broj redova 4 #1",
        "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj vidljivih celija 6 #1",
        "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj nevidljivih celija 10 #1",
        "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Vide se samo celije ispod dijagonale #1",
        "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Tabela nema redova #2",
        "fullTitle": "Tabela crtaj2() Tabela nema redova #2",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Tabela ima 1 red: expected 0 to equal 1",
          "showDiff": true,
          "actual": "0",
          "expected": "1",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela ima 1 red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:70:16)"
        }
      },
      {
        "title": "Broj redova 4 #2",
        "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj redova treba biti 3: expected 4 to equal 3",
          "showDiff": true,
          "actual": "4",
          "expected": "3",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak2.js:78:16)"
        }
      },
      {
        "title": "Broj vidljivih celija 6 #2",
        "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj vidljivih celija 10: expected 6 to equal 10",
          "showDiff": true,
          "actual": "6",
          "expected": "10",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:92:16)"
        }
      },
      {
        "title": "Broj nevidljivih celija 10 #2",
        "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Vide se samo celije ispod dijagonale #2",
        "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
          "showDiff": true,
          "actual": "true",
          "expected": "false",
          "operator": "strictEqual",
          "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak2.js:112:16)"
        }
      },
      {
        "title": "Tabela nema redova #3",
        "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Tabela ima red: expected 0 to equal 1",
          "showDiff": true,
          "actual": "0",
          "expected": "1",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:131:16)"
        }
      },
      {
        "title": "Broj redova 4 #3",
        "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj redova treba biti 2: expected 4 to equal 2",
          "showDiff": true,
          "actual": "4",
          "expected": "2",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj redova treba biti 2: expected 4 to equal 2\n    at Context.<anonymous> (zadatak2.js:139:16)"
        }
      },
      {
        "title": "Broj vidljivih celija 6 #3",
        "fullTitle": "Tabela crtaj3() Broj vidljivih celija 6 #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj vidljivih celija 10: expected 6 to equal 10",
          "showDiff": true,
          "actual": "6",
          "expected": "10",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:153:16)"
        }
      },
      {
        "title": "Broj nevidljivih celija 10 #3",
        "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
          "showDiff": true,
          "actual": "10",
          "expected": "6",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak2.js:166:16)"
        }
      },
      {
        "title": "Tabela nema redova #4",
        "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj redova 4 #4",
        "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj vidljivih celija 6 #4",
        "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:210:17)",
          "message": "brojCelijaGRESKA is not defined"
        }
      },
      {
        "title": "Broj nevidljivih celija 10 #4",
        "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:224:17)",
          "message": "brojCelijaGRESKA is not defined"
        }
      },
      {
        "title": "Vide se samo celije ispod dijagonale #4",
        "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      }
    ],
    "pending": [],
    "failures": [
      {
        "title": "Tabela nema redova #2",
        "fullTitle": "Tabela crtaj2() Tabela nema redova #2",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Tabela ima 1 red: expected 0 to equal 1",
          "showDiff": true,
          "actual": "0",
          "expected": "1",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela ima 1 red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:70:16)"
        }
      },
      {
        "title": "Broj redova 4 #2",
        "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj redova treba biti 3: expected 4 to equal 3",
          "showDiff": true,
          "actual": "4",
          "expected": "3",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak2.js:78:16)"
        }
      },
      {
        "title": "Broj vidljivih celija 6 #2",
        "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj vidljivih celija 10: expected 6 to equal 10",
          "showDiff": true,
          "actual": "6",
          "expected": "10",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:92:16)"
        }
      },
      {
        "title": "Vide se samo celije ispod dijagonale #2",
        "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
          "showDiff": true,
          "actual": "true",
          "expected": "false",
          "operator": "strictEqual",
          "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak2.js:112:16)"
        }
      },
      {
        "title": "Tabela nema redova #3",
        "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Tabela ima red: expected 0 to equal 1",
          "showDiff": true,
          "actual": "0",
          "expected": "1",
          "operator": "strictEqual",
          "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:131:16)"
        }
      },
      {
        "title": "Broj redova 4 #3",
        "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj redova treba biti 2: expected 4 to equal 2",
          "showDiff": true,
          "actual": "4",
          "expected": "2",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj redova treba biti 2: expected 4 to equal 2\n    at Context.<anonymous> (zadatak2.js:139:16)"
        }
      },
      {
        "title": "Broj vidljivih celija 6 #3",
        "fullTitle": "Tabela crtaj3() Broj vidljivih celija 6 #3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "message": "Broj vidljivih celija 10: expected 6 to equal 10",
          "showDiff": true,
          "actual": "6",
          "expected": "10",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:153:16)"
        }
      },
      {
        "title": "Broj nevidljivih celija 10 #3",
        "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
          "showDiff": true,
          "actual": "10",
          "expected": "6",
          "operator": "strictEqual",
          "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak2.js:166:16)"
        }
      },
      {
        "title": "Broj vidljivih celija 6 #4",
        "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "err": {
          "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:210:17)",
          "message": "brojCelijaGRESKA is not defined"
        }
      },
      {
        "title": "Broj nevidljivih celija 10 #4",
        "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "err": {
          "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:224:17)",
          "message": "brojCelijaGRESKA is not defined"
        }
      }
    ],
    "passes": [
      {
        "title": "Tabela nema redova #1",
        "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj redova 4 #1",
        "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj vidljivih celija 6 #1",
        "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj nevidljivih celija 10 #1",
        "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Vide se samo celije ispod dijagonale #1",
        "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj nevidljivih celija 10 #2",
        "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Tabela nema redova #4",
        "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Broj redova 4 #4",
        "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      },
      {
        "title": "Vide se samo celije ispod dijagonale #4",
        "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
      }
    ]
  }
    `
describe('TestoviParser1', function() {
 describe('dajTacnost1()',function(){
    it('svi testovi prolaze #1',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test);
        let stvarne_greske = ["Tabela crtaj2() Tabela nema redova #2"
            ,"Tabela crtaj2() Broj redova 4 #2"
            ,"Tabela crtaj2() Broj vidljivih celija 6 #2"
            ,"Tabela crtaj2() Vide se samo celije ispod dijagonale #2"
            ,"Tabela crtaj3() Tabela nema redova #3"
            ,"Tabela crtaj3() Broj redova 4 #3"
            ,"Tabela crtaj3() Broj vidljivih celija 6 #3"
            ,"Tabela crtaj3() Broj nevidljivih celija 10 #3"
            ,"Tabela crtaj4() Broj vidljivih celija 6 #4"
            ,"Tabela crtaj4() Broj nevidljivih celija 10 #4"];
        assert.equal(rezultatTacnost.tacnost, "47%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
    });
    it('svi testovi prolaze #2',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_2);
        let stvarne_greske = ["Tabela crtaj2() Broj redova 4 #2"
            ,"Tabela crtaj2() Broj vidljivih celija 6 #2"
            ,"Tabela crtaj2() Vide se samo celije ispod dijagonale #2"
            ,"Tabela crtaj3() Tabela nema redova #3"
            ,"Tabela crtaj3() Broj nevidljivih celija 10 #3"
            ,"Tabela crtaj3() Vide se samo celije ispod dijagonale #3"
            ,"Tabela crtaj4() Broj vidljivih celija 6 #4"
            ,"Tabela crtaj4() Broj nevidljivih celija 10 #4"];
        assert.equal(rezultatTacnost.tacnost, "55%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
    });
    it('svi testovi prolaze #3',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_3);
        let stvarne_greske = ["Tabela crtaj1() Tabela nema redova #1"
            ,"Tabela crtaj1() Broj redova 4 #1"
            ];
        assert.equal(rezultatTacnost.tacnost, "0%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
    });
    it('svi testovi prolaze #4',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_4);
        let stvarne_greske = [];
        assert.equal(rezultatTacnost.tacnost, "100%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
    });
    it('svi testovi prolaze #5',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_5);
        let stvarne_greske = ["Tabela crtaj1() Tabela nema redova #1"];
        assert.equal(rezultatTacnost.tacnost, "0%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
    });
 })
})
describe('TestoviParser2', function() {
 describe('dajTacnost2()',function(){
    it('jedan test prolazi #1',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test);
        let stvarne_greske = ["Tabela crtaj2() Tabela nema redova #2"
            ,"Tabela crtaj2() Broj redova 4 #2"
            ,"Tabela crtaj2() Broj vidljivih celija 6 #2"
            ,"Tabela crtaj2() Vide se samo celije ispod dijagonale #2"
            ,"Tabela crtaj3() Tabela nema redova #3"
            ,"Tabela crtaj3() Broj redova 4 #3"
            ,"Tabela crtaj3() Broj vidljivih celija 6 #3"
            ,"Tabela crtaj3() Broj nevidljivih celija 10 #3"
            ,"Tabela crtaj4() Broj vidljivih celija 6 #4"
            ,"Tabela crtaj4() Broj nevidljivih celija 10 #4"];
        assert.equal(rezultatTacnost.tacnost, "30%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
    });
    it('jedan test prolazi #2',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_2);
        let stvarne_greske = ["Tabela crtaj2() Broj redova 4 #2"
            ,"Tabela crtaj2() Broj vidljivih celija 6 #2"
            ,"Tabela crtaj2() Vide se samo celije ispod dijagonale #2"
            ,"Tabela crtaj3() Tabela nema redova #3"
            ,"Tabela crtaj3() Broj nevidljivih celija 10 #3"
            ,"Tabela crtaj3() Vide se samo celije ispod dijagonale #3"];
        assert.equal(rezultatTacnost.tacnost, "55%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
    });
    it('jedan test prolazi #3',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_3);
        let stvarne_greske = ["Tabela crtaj1() Tabela nema redova #1"
            ,"Tabela crtaj1() Broj redova 4 #1"
            ];
        assert.equal(rezultatTacnost.tacnost, "0%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
    });
    it('jedan test prolazi #4',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_4);
        let stvarne_greske = ["nesto"];
        assert.equal(rezultatTacnost.tacnost, "50%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
    });
    it('jedan test prolazi #5',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_5);
        let stvarne_greske = ["Tabela crtaj1() GRESKA edova #1"];
        assert.equal(rezultatTacnost.tacnost, "0%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
    });
 })
})

describe('TestoviParser3', function() {
    describe('dajTacnost3()',function(){
       it('nijedan test prolazi #1',function(){
           let rezultatTacnost = TestoviParser.dajTacnost(json_string_test);
           let stvarne_greske = ["Tabela crtaj2() Tabela nema redova #2"
               ,"Tabela crtaj2() Broj redova 4 #2"
               ,"Tabela crtaj2() Broj vidljivih celija 6 #2"
               ,"Tabela crtaj2() Vide se samo celije ispod dijagonale #2"
               ,"Tabela crtaj3() Tabela nema redova #3"
               ,"Tabela crtaj3() Broj redova 4 #3"
               ,"Tabela crtaj3() Broj vidljivih celija 6 #3"
               ,"Tabela crtaj3() Broj nevidljivih celija 10 #3"
               ,"Tabela crtaj4() Broj vidljivih celija 6 #4"
               ,"Tabela crtaj4() Broj nevidljivih celija 10 #4"];
           assert.equal(rezultatTacnost.tacnost, "30%", "Profulana tacnost");
           assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
       });
       it('nijedan test prolazi #2',function(){
           let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_2);
           let stvarne_greske = ["Tabela crtaj2() Broj redova 4 #2"
               ,"Tabela crtaj2() Broj vidljivih celija 6 #2"
               ,"Tabela crtaj2() Vide se samo celije ispod dijagonale #2"
               ,"Tabela crtaj3() Tabela nema redova #3"
               ,"Tabela crtaj3() Broj nevidljivih celija 10 #3"
               ,"Tabela crtaj3() Vide se samo celije ispod dijagonale #3"];
           assert.equal(rezultatTacnost.tacnost, "55%", "Profulana tacnost");
           assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
       });
       it('nijedan test prolazi #3',function(){
           let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_3);
           let stvarne_greske = ["Tabela crtaj1() Tabela nema redova #1"
               
               ];
           assert.equal(rezultatTacnost.tacnost, "5%", "Profulana tacnost");
           assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
       });
       it('nijedan test prolazi #4',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_4);
        let stvarne_greske = ["nesto"];
        assert.equal(rezultatTacnost.tacnost, "50%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
       });
       it('nijedan test prolazi #5',function(){
        let rezultatTacnost = TestoviParser.dajTacnost(json_string_test_5);
        let stvarne_greske = ["Tabela crtaj1() GRESKA edova #1"];
        assert.equal(rezultatTacnost.tacnost, "0%", "Profulana tacnost");
        assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
       });
    })
})

describe('TestoviParser4', function() {
    describe('dajTacnost4()',function(){
       it('ne mogu se izvrsiti #1',function(){
           
           let rezultatTacnost = TestoviParser.dajTacnost(nevaljasti_json_string_test);
           let stvarne_greske = "Testovi se ne mogu izvršiti";
           assert.equal(rezultatTacnost.tacnost, "0%", "Profulana tacnost");
           assert.deepEqual(rezultatTacnost.greske, stvarne_greske, "Profulane greske");
       });
    })
})