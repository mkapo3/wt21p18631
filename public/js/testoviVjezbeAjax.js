let assert = chai.assert;

describe('Testovi VjezbeAjax',function(){
    describe('Testovi dodajInputPolja',function(){
        it('Broj input polja #1',function(){
            let DOMelementDIVauFormi = document.createElement('div');
            let brojVjezbi = 4;
            VjezbeAjax.dodajInputPolja(DOMelementDIVauFormi, brojVjezbi);
            let inputPolja = DOMelementDIVauFormi.querySelectorAll('input');
            assert.equal(inputPolja.length,4,'Broj polja treba biti 4');
        })
        it('Broj input polja #2',function(){
            let DOMelementDIVauFormi = document.createElement('div');
            let brojVjezbi = 4;
            VjezbeAjax.dodajInputPolja(DOMelementDIVauFormi, brojVjezbi);
            let inputPolja = DOMelementDIVauFormi.querySelectorAll('input');
            assert.equal(inputPolja.length,4,'Broj polja treba biti 4');

            brojVjezbi = 10;
            VjezbeAjax.dodajInputPolja(DOMelementDIVauFormi, brojVjezbi);
            inputPolja = DOMelementDIVauFormi.querySelectorAll('input');
            assert.equal(inputPolja.length,10,'Broj polja treba biti 10');

        })
        it('Broj input polja #3',function(){
            let DOMelementDIVauFormi = document.createElement('div');
            let brojVjezbi = 0;
            VjezbeAjax.dodajInputPolja(DOMelementDIVauFormi, brojVjezbi);
            let inputPolja = DOMelementDIVauFormi.querySelectorAll('input');
            assert.equal(inputPolja.length,0,'Broj polja treba biti 0');
        })
        it('Vrijednost, id i naziv polja',function(){
            let DOMelementDIVauFormi = document.createElement('div');
            let brojVjezbi = 5;
            VjezbeAjax.dodajInputPolja(DOMelementDIVauFormi, brojVjezbi);
            let inputPolja = DOMelementDIVauFormi.querySelectorAll('input');

            for (let i=0; i<inputPolja.length; i++){
                assert.equal(inputPolja[i].value,4,'Vrijednost svakog polja treba biti 4');
                assert.equal(inputPolja[i].id,'z' + i, 'ID svakog polja mora biti "zi"');
                assert.equal(inputPolja[i].name,'z' + i, 'Naziv svakog polja mora biti "zi"');    
            }
        })
        it('Labele polja',function(){
            let DOMelementDIVauFormi = document.createElement('div');
            let brojVjezbi = 5;
            VjezbeAjax.dodajInputPolja(DOMelementDIVauFormi, brojVjezbi);
            let labelPolja = DOMelementDIVauFormi.querySelectorAll('label');

            for (let i=0; i<labelPolja.length; i++){
                assert.equal(labelPolja[i].innerHTML,'Zadatak ' + i,'Vrijednost svakog polja treba biti 4')
            }
        })
    })
    describe('Testovi posaljiPodatke',function(){
        beforeEach(function() {
            this.xhr = sinon.useFakeXMLHttpRequest();
         
            this.requests = [];
            this.xhr.onCreate = function(xhr) {
              this.requests.push(xhr);
            }.bind(this);
          });
         
          afterEach(function() {
            this.xhr.restore();
          });
        it('Pravilno poslani podaci #1',function(){
            let vjezbeObjekat = {
                brojVjezbi : 5,
                brojZadataka : [3,4,5,2,1]
            }
            let vjezbeObjekatlijevi = {
                brojVjezbi : 5,
                brojZadataka : [3,4,5,2,1]
            }
            function callbackFja(error,data){   
                assert.equal(error,null,'Ne treba biti errora')
                assert.deepEqual(JSON.parse(data),vjezbeObjekatlijevi,'Podaci koji su vraceni trebaju biti isti kao oni koji su poslani')
                
            }
            VjezbeAjax.posaljiPodatke(vjezbeObjekat,callbackFja);
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(vjezbeObjekat));
        })
        it('Pravilno poslani podaci #2',function(){
            //Zbog asihronog pozivanja vjezbeObjekat moraju biti dva razlicita objekta,
            //jer bi se inace vjezbeObjekat2 promijenilo prije nego sto bi drugi assert u prvoj
            //callbackFja bio pozvan
            let vjezbeObjekat = {
                brojVjezbi : 5,
                brojZadataka : [3,4,5,2,1]
            }
            function callbackFja1(error,data){   
                assert.equal(error,null,'Ne treba biti errora')
                assert.deepEqual(JSON.parse(data),vjezbeObjekat,'Podaci koji su vraceni trebaju biti isti kao oni koji su poslani')
            }
            VjezbeAjax.posaljiPodatke(vjezbeObjekat,callbackFja1);

            let vjezbeObjekat2 = {
                brojVjezbi : 3,
                brojZadataka : [3,4,5]
            }
            function callbackFja2(error,data){   
                assert.equal(error,null,'Ne treba biti errora')
                assert.deepEqual(JSON.parse(data),vjezbeObjekat2,'Podaci koji su vraceni trebaju biti isti kao oni koji su poslani')
            }
            VjezbeAjax.posaljiPodatke(vjezbeObjekat2,callbackFja2);
        })
        it('Nepravilno poslani podaci #1',function(){
            let vjezbeObjekat = {
                brojVjezbi : 16,
                brojZadataka : [3,4,5,2,5,6,7,8,9,10,9,8,7,6,5,4]
            }
            function callbackFja(error,data){   
                assert.equal(null,error,'Ne treba biti errora')
                assert.deepEqual(JSON.parse(data).data,"Pogrešan parametar brojVjezbi",'Data treba sadrzavati poruku problema')
                assert.deepEqual(JSON.parse(data).status,"error",'Status treba biti error')
            }
            VjezbeAjax.posaljiPodatke(vjezbeObjekat,callbackFja);
        })
        it('Nepravilno poslani podaci #2',function(){
            let vjezbeObjekat = {
                brojVjezbi : 5,
                brojZadataka : [11,4,5,2,1]
            }
            function callbackFja(error,data){   
                assert.equal(null,error,'Ne treba biti errora')
                assert.deepEqual(JSON.parse(data).data,"Pogrešan parametar z0",'Data treba sadrzavati poruku problema')
                assert.deepEqual(JSON.parse(data).status,"error",'Status treba biti error')
            }
            VjezbeAjax.posaljiPodatke(vjezbeObjekat,callbackFja);
        })
        it('Nepravilno poslani podaci #3',function(){
            let vjezbeObjekat = {
                brojVjezbi : 16,
                brojZadataka : [3,4,5,2,5,6,7,8,9,10,9,12,7,6,5,11]
            }
            function callbackFja(error,data){   
                assert.equal(null,error,'Ne treba biti errora')
                assert.deepEqual(JSON.parse(data).data,"Pogrešan parametar brojVjezbi,z11,z15",'Data treba sadrzavati poruku problema')
                assert.deepEqual(JSON.parse(data).status,"error",'Status treba biti error')
            }
            VjezbeAjax.posaljiPodatke(vjezbeObjekat,callbackFja);
        })
        it('Nepravilno poslani podaci #4',function(){
            let vjezbeObjekat = {
                brojVjezbi : 3,
                brojZadataka : [3,4]
            }
            function callbackFja(error,data){   
                assert.equal(null,error,'Ne treba biti errora')
                assert.deepEqual(JSON.parse(data).data,"Pogrešna velicina parametra brojZadataka",'Data treba sadrzavati poruku problema')
                assert.deepEqual(JSON.parse(data).status,"error",'Status treba biti error')
            }
            VjezbeAjax.posaljiPodatke(vjezbeObjekat,callbackFja);
        })
    })
    describe('Testovi dohvatiPodatke',function(){
        
        it('Dohvaceni podaci #1',function(){
            let vjezbeObjekat = {
                brojVjezbi : 5,
                brojZadataka : [3,4,5,2,1]
            }
            function callbackFja(error,data){   
                assert.equal(error,null,'Ne treba biti errora')
                assert.deepEqual(JSON.parse(data),vjezbeObjekat,'Podaci koji su vraceni trebaju biti isti kao oni koji su poslani')

                function callbackFjaGET(error,data){
                    assert.equal(error,null,'Ne treba biti errora kod dohvacanja');
                    assert.deepEqual(JSON.parse(data),vjezbeObjekat,'Podaci trebaju biti jednaki onima koje je poslala funkcija posaljiPodatke')
                }
                VjezbeAjax.dohvatiPodatke(callbackFjaGET);

            }
            VjezbeAjax.posaljiPodatke(vjezbeObjekat,callbackFja);
        })

        it('Dohvaceni podaci #2',function(){
            let vjezbeObjekat = {
                brojVjezbi : 3,
                brojZadataka : [1,2,3]
            }
            function callbackFja(error,data){   
                assert.equal(error,null,'Ne treba biti errora')
                assert.deepEqual(JSON.parse(data),vjezbeObjekat,'Podaci koji su vraceni trebaju biti isti kao oni koji su poslani')

                function callbackFjaGET(error,data){
                    assert.equal(error,null,'Ne treba biti errora kod dohvacanja');
                    assert.deepEqual(JSON.parse(data),vjezbeObjekat,'Podaci trebaju biti jednaki onima koje je poslala funkcija posaljiPodatke')
                }
                VjezbeAjax.dohvatiPodatke(callbackFjaGET);

            }
            //Stavljen timeout zbog testa iznad.
            //Kada nema timeout, posalju se podaci iz 
            //ovog testa prije nego sto prethodni uspije dobiti svoje podatake
            setTimeout(function(){VjezbeAjax.posaljiPodatke(vjezbeObjekat,callbackFja)},2*1000);
        })
    })
    describe('Testovi iscrtajVjezbe',function(){
        it('Broj vjezbi',function(){
            let divDOMelement = document.createElement('div');
            let vjezbeObjekat = {
                brojVjezbi : 3,
                brojZadataka : [1,2,3]
            }
            VjezbeAjax.iscrtajVjezbe(divDOMelement, vjezbeObjekat);
            let sveVjezbe = divDOMelement.querySelectorAll('.vjezba');
            assert.equal(sveVjezbe.length, 3, 'Broj vjezbi treba biti 3');
        })
        it('Sadrzaj vjezbi',function(){
            let divDOMelement = document.createElement('div');
            let vjezbeObjekat = {
                brojVjezbi : 3,
                brojZadataka : [1,2,3]
            }
            VjezbeAjax.iscrtajVjezbe(divDOMelement, vjezbeObjekat);
            let sveVjezbe = divDOMelement.querySelectorAll('.vjezba');
            for (let i=0; i<sveVjezbe.length; i++){
                assert.equal(sveVjezbe[i].querySelector('h3').innerHTML, 'Vježba ' + i, 'Sadrzaj vjezbe treba biti Vježbai');
            }
            assert.equal(sveVjezbe.length, 3, 'Broj vjezbi treba biti 3');
        })
        it('onClick metoda vjezbi',function(){
            let divDOMelement = document.createElement('div');
            let vjezbeObjekat = {
                brojVjezbi : 3,
                brojZadataka : [1,2,3]
            }
            VjezbeAjax.iscrtajVjezbe(divDOMelement, vjezbeObjekat);
            let sveVjezbe = divDOMelement.querySelectorAll('.vjezba');
            for (let i=0; i<sveVjezbe.length; i++){
                assert.equal(sveVjezbe[i].getAttribute('onClick'), 'iscrtajZadatke(this,'+vjezbeObjekat.brojZadataka[i] +')', 'Sadrzaj vjezbe treba biti Vježbai');
            }
            assert.equal(sveVjezbe.length, 3, 'Broj vjezbi treba biti 3');
        })

    })
    describe('Testovi iscrtajZadatke',function(){
        it('Broj zadataka samo jedne vjezbe',function(){
            let divDOMelement = document.createElement('div');
            let vjezbeObjekat = {
                brojVjezbi : 3,
                brojZadataka : [1,2,3]
            }
            VjezbeAjax.iscrtajVjezbe(divDOMelement, vjezbeObjekat);
            VjezbeAjax.iscrtajZadatke(divDOMelement.querySelectorAll('.vjezba')[0], vjezbeObjekat.brojZadataka[0]);
            let prvaVjezba = divDOMelement.querySelectorAll('.vjezba')[0];
            let sviZadaciPrve = prvaVjezba.querySelectorAll('.zadatak');

            assert.equal(sviZadaciPrve.length,1,'Broj zadataka prve vjezbe treba biti 1');
        })
        it('Broj zadataka svih vjezbi #1',function(){
            let divDOMelement = document.createElement('div');
            let vjezbeObjekat = {
                brojVjezbi : 3,
                brojZadataka : [1,2,3]
            }
            VjezbeAjax.iscrtajVjezbe(divDOMelement, vjezbeObjekat);
            let sveVjezbe = divDOMelement.querySelectorAll('.vjezba');
            
            for(let i=0; i<sveVjezbe.length; i++){
                VjezbeAjax.iscrtajZadatke(sveVjezbe[i], vjezbeObjekat.brojZadataka[i]);
                let vjezba = divDOMelement.querySelectorAll('.vjezba')[i];
                let sviZadaci = vjezba.querySelectorAll('.zadatak');
                assert.equal(sviZadaci.length,vjezbeObjekat.brojZadataka[i],'Broj zadataka prve vjezbe treba biti ' + vjezbeObjekat.brojZadataka[i]);
            }
        })

        it('Broj zadataka svih vjezbi #2',function(){
            let divDOMelement = document.createElement('div');
            let vjezbeObjekat = {
                brojVjezbi : 2,
                brojZadataka : [0,1]
            }
            VjezbeAjax.iscrtajVjezbe(divDOMelement, vjezbeObjekat);
            let sveVjezbe = divDOMelement.querySelectorAll('.vjezba');
            
            for(let i=0; i<sveVjezbe.length; i++){
                VjezbeAjax.iscrtajZadatke(sveVjezbe[i], vjezbeObjekat.brojZadataka[i]);
                let vjezba = divDOMelement.querySelectorAll('.vjezba')[i];
                let sviZadaci = vjezba.querySelectorAll('.zadatak');
                assert.equal(sviZadaci.length,vjezbeObjekat.brojZadataka[i],'Broj zadataka prve vjezbe treba biti ' + vjezbeObjekat.brojZadataka[i]);
            }
        })
    })

})