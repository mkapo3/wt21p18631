function ispisi(error,data){
    if(!error){
        console.log(data);
    }
    else{
        console.log(error);
    }
}

document.getElementById("brojVjezbi").onblur = function(){
    let brojVjezbi = document.getElementById("brojVjezbi").value;
    let zadaciDiv = document.getElementById("zadaciDiv");
    if(brojVjezbi < 16){
        VjezbeAjax.dodajInputPolja(zadaciDiv,brojVjezbi);   
    }
    else{
        document.getElementById("brojVjezbi").value = "Ne moze vise od 15";
    }
}

document.getElementById("posaljiButton").addEventListener("click",function(){
    let zadaci = document.getElementsByClassName("zadatak");
    let brojVjezbi = document.getElementById("brojVjezbi").value;
    let brojZadataka = [];
    let nepravilno = false;
    for (let i=0; i<zadaci.length; i++){
        if(zadaci[i].value>10){
            nepravilno = true;
            zadaci[i].value = "Ne moze vise od 10";
            break;
        }
        brojZadataka.push(zadaci[i].value);
    }


    let vjezbeObjekat = {
        brojVjezbi : brojVjezbi,
        brojZadataka : brojZadataka
    }
    if(!nepravilno){
        VjezbeAjax.posaljiPodatke(vjezbeObjekat, ispisi);
    }
    else{
        console.log("Broj zadataka ne moze biti veci od 10");
    }
})