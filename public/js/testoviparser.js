var TestoviParser = (function(){

    var dajTacnost = function(json_string){
        json_string = json_string.replaceAll("\n","");
        try{
          const json_object = JSON.parse(json_string);
          let tacnostBroj = parseFloat(json_object.stats.passes)/parseFloat(json_object.stats.tests);
          tacnostBroj*=100;
          tacnost = parseInt(tacnostBroj).toString() + "%";
          let failures = json_object.failures;
          let fullTitles = [];
          for(let i=0; i<failures.length; i++){
                fullTitles.push(failures[i].fullTitle);
          }
          return{
              tacnost: tacnost,
              greske: fullTitles
          }
        }
        catch(e){
          return {
            tacnost: "0%",
            greske: "Testovi se ne mogu izvršiti"
          }
        }
        
    }

    var porediRezultate = function(rezultat1, rezultat2){
      
      rezultat1 = rezultat1.replaceAll("\n","");
      rezultat2 = rezultat2.replaceAll("\n","");
      prvi_rezultat = JSON.parse(rezultat1);
      drugi_rezultat = JSON.parse(rezultat2);
      prvi_rezultat_testovi = prvi_rezultat.tests.map(test => test.fullTitle);
      drugi_rezultat_testovi = drugi_rezultat.tests.map(test => test.fullTitle);

      let x=0;
      let greske = [];
      if(JSON.stringify(prvi_rezultat_testovi)===JSON.stringify(drugi_rezultat_testovi)){
        x = dajTacnost(rezultat2).tacnost;
        let greske_rezultat2 = drugi_rezultat.failures.map(greska => greska.fullTitle);
        greske_rezultat2 = greske_rezultat2.sort((string1, string2) => string1<string2);
        greske = greske_rezultat2;
      }
      else{
        let testovi_rezultat1 = prvi_rezultat.tests.map(test => test.fullTitle);
        let testovi_rezultat2 = drugi_rezultat.tests.map(test => test.fullTitle);
        let greske_rezultat1 = prvi_rezultat.failures.map(greska => greska.fullTitle);
        let greske_rezultat2 = drugi_rezultat.failures.map(greska => greska.fullTitle);
        
        let razlicite_greske_rezultat1 = greske_rezultat1.filter(x => !testovi_rezultat2.includes(x));
        x = parseInt((razlicite_greske_rezultat1.length + greske_rezultat2.length)/(razlicite_greske_rezultat1.length + testovi_rezultat2.length)*100).toString() + "%";
        
        razlicite_greske_rezultat1 = razlicite_greske_rezultat1.sort((string1, string2) => string1<string2);
        let razlicite_greske_rezultat2 = greske_rezultat2.filter(x => !testovi_rezultat1.includes(x));
        razlicite_greske_rezultat2 = razlicite_greske_rezultat2.sort((string1, string2) => string1<string2);
        
        greske.push.apply(greske,razlicite_greske_rezultat1.concat(razlicite_greske_rezultat2));
        
      }

      return{
        promjena: x,
        greske: greske
      }
    }

    return {
        dajTacnost : dajTacnost,
        porediRezultate : porediRezultate
    }
}());
{
let json_string_test = `{
  "stats": {
    "suites": 5,
    "tests": 19,
    "passes": 9,
    "pending": 0,
    "failures": 10,
    "start": "2021-11-17T18:40:46.180Z",
    "end": "2021-11-17T18:40:46.189Z",
    "duration": 9
  },
  "tests": [
    {
      "title": "Tabela nema redova #1",
      "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj redova 4 #1",
      "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj vidljivih celija 6 #1",
      "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
      "file": null,
      "duration": 1,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj nevidljivih celija 10 #1",
      "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Vide se samo celije ispod dijagonale #1",
      "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Tabela nema redova #2",
      "fullTitle": "Tabela crtaj2() Tabela nema redova #2",
      "file": null,
      "duration": 1,
      "currentRetry": 0,
      "err": {
        "message": "Tabela ima 1 red: expected 0 to equal 1",
        "showDiff": true,
        "actual": "0",
        "expected": "1",
        "operator": "strictEqual",
        "stack": "AssertionError: Tabela ima 1 red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:70:16)"
      }
    },
    {
      "title": "Broj redova 4 #2",
      "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj redova treba biti 3: expected 4 to equal 3",
        "showDiff": true,
        "actual": "4",
        "expected": "3",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak2.js:78:16)"
      }
    },
    {
      "title": "Broj vidljivih celija 6 #2",
      "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj vidljivih celija 10: expected 6 to equal 10",
        "showDiff": true,
        "actual": "6",
        "expected": "10",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:92:16)"
      }
    },
    {
      "title": "Broj nevidljivih celija 10 #2",
      "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Vide se samo celije ispod dijagonale #2",
      "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
        "showDiff": true,
        "actual": "true",
        "expected": "false",
        "operator": "strictEqual",
        "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak2.js:112:16)"
      }
    },
    {
      "title": "Tabela nema redova #3",
      "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Tabela ima red: expected 0 to equal 1",
        "showDiff": true,
        "actual": "0",
        "expected": "1",
        "operator": "strictEqual",
        "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:131:16)"
      }
    },
    {
      "title": "Broj redova 4 #3",
      "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj redova treba biti 2: expected 4 to equal 2",
        "showDiff": true,
        "actual": "4",
        "expected": "2",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj redova treba biti 2: expected 4 to equal 2\n    at Context.<anonymous> (zadatak2.js:139:16)"
      }
    },
    {
      "title": "Broj vidljivih celija 6 #3",
      "fullTitle": "Tabela crtaj3() Broj vidljivih celija 6 #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj vidljivih celija 10: expected 6 to equal 10",
        "showDiff": true,
        "actual": "6",
        "expected": "10",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:153:16)"
      }
    },
    {
      "title": "Broj nevidljivih celija 10 #3",
      "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
      "file": null,
      "duration": 1,
      "currentRetry": 0,
      "err": {
        "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
        "showDiff": true,
        "actual": "10",
        "expected": "6",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak2.js:166:16)"
      }
    },
    {
      "title": "Tabela nema redova #4",
      "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj redova 4 #4",
      "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj vidljivih celija 6 #4",
      "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:210:17)",
        "message": "brojCelijaGRESKA is not defined"
      }
    },
    {
      "title": "Broj nevidljivih celija 10 #4",
      "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
      "file": null,
      "duration": 1,
      "currentRetry": 0,
      "err": {
        "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:224:17)",
        "message": "brojCelijaGRESKA is not defined"
      }
    },
    {
      "title": "Vide se samo celije ispod dijagonale #4",
      "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    }
  ],
  "pending": [],
  "failures": [
    {
      "title": "Tabela nema redova #2",
      "fullTitle": "Tabela crtaj2() Tabela nema redova #2",
      "file": null,
      "duration": 1,
      "currentRetry": 0,
      "err": {
        "message": "Tabela ima 1 red: expected 0 to equal 1",
        "showDiff": true,
        "actual": "0",
        "expected": "1",
        "operator": "strictEqual",
        "stack": "AssertionError: Tabela ima 1 red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:70:16)"
      }
    },
    {
      "title": "Broj redova 4 #2",
      "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj redova treba biti 3: expected 4 to equal 3",
        "showDiff": true,
        "actual": "4",
        "expected": "3",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak2.js:78:16)"
      }
    },
    {
      "title": "Broj vidljivih celija 6 #2",
      "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj vidljivih celija 10: expected 6 to equal 10",
        "showDiff": true,
        "actual": "6",
        "expected": "10",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:92:16)"
      }
    },
    {
      "title": "Vide se samo celije ispod dijagonale #2",
      "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
        "showDiff": true,
        "actual": "true",
        "expected": "false",
        "operator": "strictEqual",
        "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak2.js:112:16)"
      }
    },
    {
      "title": "Tabela nema redova #3",
      "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Tabela ima red: expected 0 to equal 1",
        "showDiff": true,
        "actual": "0",
        "expected": "1",
        "operator": "strictEqual",
        "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:131:16)"
      }
    },
    {
      "title": "Broj redova 4 #3",
      "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj redova treba biti 2: expected 4 to equal 2",
        "showDiff": true,
        "actual": "4",
        "expected": "2",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj redova treba biti 2: expected 4 to equal 2\n    at Context.<anonymous> (zadatak2.js:139:16)"
      }
    },
    {
      "title": "Broj vidljivih celija 6 #3",
      "fullTitle": "Tabela crtaj3() Broj vidljivih celija 6 #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj vidljivih celija 10: expected 6 to equal 10",
        "showDiff": true,
        "actual": "6",
        "expected": "10",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:153:16)"
      }
    },
    {
      "title": "Broj nevidljivih celija 10 #3",
      "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
      "file": null,
      "duration": 1,
      "currentRetry": 0,
      "err": {
        "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
        "showDiff": true,
        "actual": "10",
        "expected": "6",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak2.js:166:16)"
      }
    },
    {
      "title": "Broj vidljivih celija 6 #4",
      "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:210:17)",
        "message": "brojCelijaGRESKA is not defined"
      }
    },
    {
      "title": "Broj nevidljivih celija 10 #4",
      "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
      "file": null,
      "duration": 1,
      "currentRetry": 0,
      "err": {
        "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:224:17)",
        "message": "brojCelijaGRESKA is not defined"
      }
    }
  ],
  "passes": [
    {
      "title": "Tabela nema redova #1",
      "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj redova 4 #1",
      "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj vidljivih celija 6 #1",
      "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
      "file": null,
      "duration": 1,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj nevidljivih celija 10 #1",
      "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Vide se samo celije ispod dijagonale #1",
      "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj nevidljivih celija 10 #2",
      "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Tabela nema redova #4",
      "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj redova 4 #4",
      "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Vide se samo celije ispod dijagonale #4",
      "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    }
  ]
}
  `
let json_string_test_2 = `{
  "stats": {
    "suites": 5,
    "tests": 18,
    "passes": 10,
    "pending": 0,
    "failures": 8,
    "start": "2021-11-17T18:43:17.331Z",
    "end": "2021-11-17T18:43:17.340Z",
    "duration": 9
  },
  "tests": [
    {
      "title": "Tabela nema redova #1",
      "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj redova 4 #1",
      "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj vidljivih celija 6 #1",
      "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj nevidljivih celija 10 #1",
      "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
      "file": null,
      "duration": 1,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Vide se samo celije ispod dijagonale #1",
      "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj redova 4 #2",
      "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj redova treba biti 3: expected 4 to equal 3",
        "showDiff": true,
        "actual": "4",
        "expected": "3",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak3pomocni.js:78:16)"
      }
    },
    {
      "title": "Broj vidljivih celija 6 #2",
      "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj vidljivih celija 10: expected 6 to equal 10",
        "showDiff": true,
        "actual": "6",
        "expected": "10",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak3pomocni.js:92:16)"
      }
    },
    {
      "title": "Broj nevidljivih celija 10 #2",
      "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Vide se samo celije ispod dijagonale #2",
      "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
        "showDiff": true,
        "actual": "true",
        "expected": "false",
        "operator": "strictEqual",
        "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:112:16)"
      }
    },
    {
      "title": "Tabela nema redova #3",
      "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Tabela ima red: expected 0 to equal 1",
        "showDiff": true,
        "actual": "0",
        "expected": "1",
        "operator": "strictEqual",
        "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak3pomocni.js:131:16)"
      }
    },
    {
      "title": "Broj redova 4 #3",
      "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj nevidljivih celija 10 #3",
      "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
        "showDiff": true,
        "actual": "10",
        "expected": "6",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak3pomocni.js:166:16)"
      }
    },
    {
      "title": "Vide se samo celije ispod dijagonale #3",
      "fullTitle": "Tabela crtaj3() Vide se samo celije ispod dijagonale #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
        "showDiff": true,
        "actual": "true",
        "expected": "false",
        "operator": "strictEqual",
        "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:173:16)"
      }
    },
    {
      "title": "Tabela nema redova #4",
      "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj redova 4 #4",
      "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj vidljivih celija 6 #4",
      "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:210:17)",
        "message": "brojCelijaGRESKA is not defined"
      }
    },
    {
      "title": "Broj nevidljivih celija 10 #4",
      "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:224:17)",
        "message": "brojCelijaGRESKA is not defined"
      }
    },
    {
      "title": "Vide se samo celije ispod dijagonale #4",
      "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    }
  ],
  "pending": [],
  "failures": [
    {
      "title": "Broj redova 4 #2",
      "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj redova treba biti 3: expected 4 to equal 3",
        "showDiff": true,
        "actual": "4",
        "expected": "3",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak3pomocni.js:78:16)"
      }
    },
    {
      "title": "Broj vidljivih celija 6 #2",
      "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj vidljivih celija 10: expected 6 to equal 10",
        "showDiff": true,
        "actual": "6",
        "expected": "10",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak3pomocni.js:92:16)"
      }
    },
    {
      "title": "Vide se samo celije ispod dijagonale #2",
      "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
        "showDiff": true,
        "actual": "true",
        "expected": "false",
        "operator": "strictEqual",
        "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:112:16)"
      }
    },
    {
      "title": "Tabela nema redova #3",
      "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Tabela ima red: expected 0 to equal 1",
        "showDiff": true,
        "actual": "0",
        "expected": "1",
        "operator": "strictEqual",
        "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak3pomocni.js:131:16)"
      }
    },
    {
      "title": "Broj nevidljivih celija 10 #3",
      "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
        "showDiff": true,
        "actual": "10",
        "expected": "6",
        "operator": "strictEqual",
        "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak3pomocni.js:166:16)"
      }
    },
    {
      "title": "Vide se samo celije ispod dijagonale #3",
      "fullTitle": "Tabela crtaj3() Vide se samo celije ispod dijagonale #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
        "showDiff": true,
        "actual": "true",
        "expected": "false",
        "operator": "strictEqual",
        "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:173:16)"
      }
    },
    {
      "title": "Broj vidljivih celija 6 #4",
      "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:210:17)",
        "message": "brojCelijaGRESKA is not defined"
      }
    },
    {
      "title": "Broj nevidljivih celija 10 #4",
      "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "err": {
        "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:224:17)",
        "message": "brojCelijaGRESKA is not defined"
      }
    }
  ],
  "passes": [
    {
      "title": "Tabela nema redova #1",
      "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj redova 4 #1",
      "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj vidljivih celija 6 #1",
      "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj nevidljivih celija 10 #1",
      "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
      "file": null,
      "duration": 1,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Vide se samo celije ispod dijagonale #1",
      "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj nevidljivih celija 10 #2",
      "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj redova 4 #3",
      "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Tabela nema redova #4",
      "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Broj redova 4 #4",
      "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    },
    {
      "title": "Vide se samo celije ispod dijagonale #4",
      "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
      "file": null,
      "duration": 0,
      "currentRetry": 0,
      "speed": "fast",
      "err": {}
    }
  ]
}`  

let rezultatTestiranja = TestoviParser.dajTacnost(json_string_test);
let rezultatPoredjenja = TestoviParser.porediRezultate(json_string_test,json_string_test);
console.log(rezultatTestiranja);
console.log(rezultatPoredjenja);
}