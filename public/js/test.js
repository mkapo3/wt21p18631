let assert = chai.assert;
describe('Tabela', function() {
 describe('crtaj1()',function(){
    it('Tabela nema redova #1',function(){
        Tabela.crtaj(0,0);
        let tabele = document.getElementsByTagName('table');
        let tabela = tabele[tabele.length-1];
        let redovi = tabela.getElementsByTagName('tr');
        assert.equal(redovi.length,3,"Tabela nema redova");
        
    })
    it('Broj redova 4 #1',function(){
        Tabela.crtaj(4,4);
        let tabele = document.getElementsByTagName('table');
        let tabela = tabele[tabele.length-1];
        let redovi = tabela.getElementsByTagName('tr');
        assert.equal(redovi.length,4,"Broj redova treba biti 4");
    })
    it('Broj vidljivih celija 6 #1',function(){
        Tabela.crtaj(4,4);
        let tabele = document.getElementsByTagName('table');
        let tabela = tabele[tabele.length-1];
        let celije = tabela.getElementsByTagName('td');
        let brojCelija = 0;
        for(let i=0; i<celije.length; i++){
            if(celije[i].style.display!=='none'){
                brojCelija++;
            }
        } 
        assert.equal(brojCelija,6,"Broj vidljivih celija 6");
    })
    it('Broj nevidljivih celija 10 #1',function(){
        Tabela.crtaj(4,4);
        let tabele = document.getElementsByTagName('table');
        let tabela = tabele[tabele.length-1];
        let celije = tabela.getElementsByTagName('td');
        let brojCelija = 0;
        for(let i=0; i<celije.length; i++){
            if(celije[i].style.display==='none'){
                brojCelija++;
            }
        }
        assert.equal(brojCelija,10,"Broj nevidljivih celija 10");
    });
    it('Vide se samo celije ispod dijagonale #1',function(){
        Tabela.crtaj(5,5);
        let tabele = document.getElementsByTagName('table');
        let tabela = tabele[tabele.length-1];
        let redovi = tabela.getElementsByTagName('tr');
        assert.equal(function(){
        for(let i=0; i<redovi.length; i++){
            let celije = redovi[i].getElementsByTagName('td');
            for(let j=i; j<celije.length; j++){
                    if(celije[j].style.display!=='none'){
                        return false;
                    }
                }
            }
            return true;
        }(),true,"Celije trebaju biti samo ispod glavne dijagonale");
    })
 })
//  describe('crtaj2()',function(){
//     it('Tabela nema redova #2',function(){
//         Tabela.crtaj(0,0);
//         let tabele = document.getElementsByTagName('table');
//         let tabela = tabele[tabele.length-1];
//         let redovi = tabela.getElementsByTagName('tr');
//         assert.equal(redovi.length,1,"Tabela ima 1 red");
        
//     }); 
//     it('Broj redova 4 #2',function(){
//         Tabela.crtaj(4,4);
//         let tabele = document.getElementsByTagName('table');
//         let tabela = tabele[tabele.length-1];
//         let redovi = tabela.getElementsByTagName('tr');
//         assert.equal(redovi.length,3,"Broj redova treba biti 3");
//     });
//     it('Broj vidljivih celija 6 #2',function(){
//         Tabela.crtaj(4,4);
//         let tabele = document.getElementsByTagName('table');
//         let tabela = tabele[tabele.length-1];
//         let celije = tabela.getElementsByTagName('td');
//         let brojCelija = 0;
//         for(let i=0; i<celije.length; i++){
//             if(celije[i].style.display!=='none'){
//                 brojCelija++;
//             }
//         }
        
//         assert.equal(brojCelija,10,"Broj vidljivih celija 10");
//     });
//     it('Broj nevidljivih celija 10 #2',function(){
//         Tabela.crtaj(4,4);
//         let tabele = document.getElementsByTagName('table');
//         let tabela = tabele[tabele.length-1];
//         let celije = tabela.getElementsByTagName('td');
//         let brojCelija = 0;
//         for(let i=0; i<celije.length; i++){
//             if(celije[i].style.display==='none'){
//                 brojCelija++;
//             }
//         }
//         assert.equal(brojCelija,10,"Broj nevidljivih celija 10");
//     });
//     it('Vide se samo celije ispod dijagonale #2',function(){
//         Tabela.crtaj(5,5);
//         let tabele = document.getElementsByTagName('table');
//         let tabela = tabele[tabele.length-1];
//         let redovi = tabela.getElementsByTagName('tr');
//         assert.equal(function(){
//         for(let i=0; i<redovi.length; i++){
//             let celije = redovi[i].getElementsByTagName('td');
//             for(let j=i; j<celije.length; j++){
//                     if(celije[j].style.display!=='none'){
//                         return false;
//                     }
//                 }
//             }
//             return true;
//         }(),false,"Celije trebaju biti samo ispod glavne dijagonale");
//     })
//  })
//  describe('crtaj3()',function(){
//     it('Tabela nema redova #3',function(){
//         Tabela.crtaj(0,0);
//         let tabele = document.getElementsByTagName('table');
//         let tabela = tabele[tabele.length-1];
//         let redovi = tabela.getElementsByTagName('tr');
//         assert.equal(redovi.length,1,"Tabela ima red");
        
//     }); 
//     it('Broj redova 4 #3',function(){
//         Tabela.crtaj(4,4);
//         let tabele = document.getElementsByTagName('table');
//         let tabela = tabele[tabele.length-1];
//         let redovi = tabela.getElementsByTagName('tr');
//         assert.equal(redovi.length,4,"Broj redova treba biti 4");
//     });
//     it('Broj vidljivih celija 6 #3',function(){
//         Tabela.crtaj(4,4);
//         let tabele = document.getElementsByTagName('table');
//         let tabela = tabele[tabele.length-1];
//         let celije = tabela.getElementsByTagName('td');
//         let brojCelija = 0;
//         for(let i=0; i<celije.length; i++){
//             if(celije[i].style.display!=='none'){
//                 brojCelija++;
//             }
//         }
        
//         assert.equal(brojCelija,10,"Broj vidljivih celija 10");
//     });
//     it('Broj nevidljivih celija 10 #3',function(){
//         Tabela.crtaj(4,4);
//         let tabele = document.getElementsByTagName('table');
//         let tabela = tabele[tabele.length-1];
//         let celije = tabela.getElementsByTagName('td');
//         let brojCelija = 0;
//         for(let i=0; i<celije.length; i++){
//             if(celije[i].style.display==='none'){
//                 brojCelija++;
//             }
//         }
//         assert.equal(brojCelija,6,"Broj nevidljivih celija 6");
//     });
//     it('Vide se samo celije ispod dijagonale #3',function(){
//         Tabela.crtaj(5,5);
//         let tabele = document.getElementsByTagName('table');
//         let tabela = tabele[tabele.length-1];
//         let redovi = tabela.getElementsByTagName('tr');
//         assert.equal(function(){
//         for(let i=0; i<redovi.length; i++){
//             let celije = redovi[i].getElementsByTagName('td');
//             for(let j=i; j<celije.length; j++){
//                     if(celije[j].style.display!=='none'){
//                         return false;
//                     }
//                 }
//             }
//             return true;
//         }(),false,"Celije trebaju biti samo ispod glavne dijagonale");
//     })
//  })
 describe('crtaj4()',function(){
    it('Tabela nema redova #4',function(){
        Tabela.crtaj(0,0);
        let tabele = document.getElementsByTagName('table');
        let tabela = tabele[tabele.length-1];
        let redovi = tabela.getElementsByTagName('tr');
        assert.equal(redovi.length,0,"Tabela nema redova");
        
    }); 
    it('Broj redova 4 #4',function(){
        Tabela.crtaj(4,4);
        let tabele = document.getElementsByTagName('table');
        let tabela = tabele[tabele.length-1];
        let redovi = tabela.getElementsByTagName('tr');
        assert.equal(redovi.length,4,"Broj redova treba biti 4");
    });
    it('Broj vidljivih celija 6 #4',function(){
        Tabela.crtaj(4,4);
        let tabele = document.getElementsByTagName('table');
        let tabela = tabele[tabele.length-1];
        let celije = tabela.getElementsByTagName('td');
        let brojCelija = 0;
        for(let i=0; i<celije.length; i++){
            if(celije[i].style.display!=='none'){
                brojCelijaGRESKA++;
            }
        }
        
        assert.equal(brojCelijaGRESKA,10,"Broj vidljivih celija 10");
    });
    it('Broj nevidljivih celija 10 #4',function(){
        Tabela.crtaj(4,4);
        let tabele = document.getElementsByTagName('table');
        let tabela = tabele[tabele.length-1];
        let celije = tabela.getElementsByTagName('td');
        let brojCelija = 0;
        for(let i=0; i<celije.length; i++){
            if(celije[i].style.display==='none'){
                brojCelijaGRESKA++;
            }
        }
        assert.equal(brojCelijaGRESKA,6,"Broj nevidljivih celija 6");
    });
    it('Vide se samo celije ispod dijagonale #4',function(){
        Tabela.crtaj(5,5);
        let tabele = document.getElementsByTagName('table');
        let tabela = tabele[tabele.length-1];
        let redovi = tabela.getElementsByTagName('tr');
        assert.equal(function(){
        for(let i=0; i<redovi.length; i++){
            let celije = redovi[i].getElementsByTagName('td');
            for(let j=i; j<celije.length; j++){
                    if(celije[j].style.display!=='none'){
                        return false;
                    }
                }
            }
            return true;
        }(),true,"Celije trebaju biti samo ispod glavne dijagonale");
    })
 })
});
