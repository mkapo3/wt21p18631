var StudentAjax = (function(){

    var dodajStudenta = function(student,fnCallback){
        let ajax = new XMLHttpRequest();

        ajax.onreadystatechange = function(){
            if(ajax.readyState == 4 && ajax.status == 200){
                fnCallback(null,ajax.responseText);
            }
            else if(ajax.readyState == 4){
                fnCallback(ajax.statusText,null);
                console.log("Nesto ne valja sa dodavanjem studenta u ajaxu");
            }
        }

        ajax.open("POST","http://localhost:3000/student",true);
        ajax.setRequestHeader("Content-Type","application/json");
        ajax.send(JSON.stringify(student));
    }

    var postaviGrupu = function(index,grupa,fnCallback){
        let ajax = new XMLHttpRequest();

        ajax.onreadystatechange = function(){
            if(ajax.readyState == 4 && ajax.status == 200){
                fnCallback(null,ajax.responseText);
            }
            else if(ajax.readyState == 4){
                fnCallback(ajax.statusText,null);
                console.log("Nesto ne valja sa dodavanjem studenta u ajaxu");
            }
        }

        ajax.open("PUT","http://localhost:3000/student/"+index,true);
        ajax.setRequestHeader("Content-Type","application/json");
        ajax.send(JSON.stringify({grupa: grupa}));
    }

    var dodajBatch = function(csvStudenti, fnCallback){
        let ajax = new XMLHttpRequest();

        ajax.onreadystatechange = function(){
            if(ajax.readyState == 4 && ajax.status == 200){
                fnCallback(null,ajax.responseText);
            }
            else if(ajax.readyState == 4){
                fnCallback(ajax.statusText,null);
                console.log("Nesto ne valja sa dodavanjem studenta u ajaxu");
            }
        }

        ajax.open("POST","http://localhost:3000/batch/student",true);
        ajax.setRequestHeader("Content-Type","application/raw");
        ajax.send(csvStudenti);
    }

    return {
        dodajStudenta : dodajStudenta,
        postaviGrupu : postaviGrupu,
        dodajBatch : dodajBatch
    }

}());