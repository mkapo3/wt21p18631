var VjezbeAjax = (function(){

    var dodajInputPolja = function(DOMelementDIVauFormi, brojVjezbi){
        DOMelementDIVauFormi.innerHTML = "";
        
        for (let i=0; i<brojVjezbi; i++){
            DOMelementDIVauFormi.innerHTML = DOMelementDIVauFormi.innerHTML + 
            '<label for="z'+ i + '">Zadatak ' + i + '</label><br>' +
            '<input class="zadatak" type="text" name="z'+ i + '" id="z' + i +'" value=4><br>';
        }
    }

    var posaljiPodatke = function(vjezbeObjekat, callbackFja){
        var ajax = new XMLHttpRequest();

        ajax.onreadystatechange = function(){
            if(ajax.readyState == 4 && ajax.status == 200){
                callbackFja(null,ajax.responseText);
            }
            else if(ajax.readyState == 4){
                console.log("Problem sa slanjem POST-a");
                callbackFja(ajax.statusText,null);
            }

        }
        
        ajax.open("POST","http://localhost:3000/vjezbe",true);
        ajax.setRequestHeader("Content-Type","application/json");
        ajax.send(JSON.stringify(vjezbeObjekat));
    }

    var dohvatiPodatke = function(callbackFja){
        let ajax = new XMLHttpRequest();

        ajax.onreadystatechange = function(){
            if(ajax.readyState == 4 && ajax.status == 200){
                callbackFja(null, ajax.responseText);
            }
            else if(ajax.readyState == 4){
                callbackFja(ajax.statusText, null);
            }
        }
        
        ajax.open("GET","http://localhost:3000/vjezbe",true);
        ajax.send();
    }

    var iscrtajVjezbe = function(divDOMelement, vjezbeObjekat){
        let brojVjezbi = vjezbeObjekat.brojVjezbi;
        let brojZadataka = vjezbeObjekat.brojZadataka;

        for(let i=0; i<brojVjezbi; i++){
            divDOMelement.innerHTML = divDOMelement.innerHTML +
            '<div class="vjezba" onClick="iscrtajZadatke(this,'+ brojZadataka[i] + ')">' + 
            '<h3>Vježba ' + i + '</h3>' + '</div>';
        }
    }

    var iscrtajZadatke = function(vjezbaDOMelement, brojZadataka){
        
        let zadaci = "";
        for (let j=0; j<brojZadataka; j++){
            zadaci = zadaci +
            '<div class="zadatak">Zadatak ' + j + '</div>'
        }
        vjezbaDOMelement.innerHTML = vjezbaDOMelement.innerHTML +
        '<div class="zadaci">' + zadaci + '</div>'
        
    }

    return {
        dodajInputPolja : dodajInputPolja,
        posaljiPodatke : posaljiPodatke,
        dohvatiPodatke : dohvatiPodatke,
        iscrtajVjezbe : iscrtajVjezbe,
        iscrtajZadatke : iscrtajZadatke
    }
}());