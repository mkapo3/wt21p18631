let assert = chai.assert;
describe ('PorediRezultate', function(){
    describe('funkcija', function(){
        it('Jednaki ulazni rezultati', function(){
            let json_string_test = `{
                "stats": {
                  "suites": 5,
                  "tests": 19,
                  "passes": 9,
                  "pending": 0,
                  "failures": 10,
                  "start": "2021-11-17T18:40:46.180Z",
                  "end": "2021-11-17T18:40:46.189Z",
                  "duration": 9
                },
                "tests": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #1",
                    "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #1",
                    "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Tabela nema redova #2",
                    "fullTitle": "Tabela crtaj2() Tabela nema redova #2",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima 1 red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima 1 red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:70:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #2",
                    "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 3: expected 4 to equal 3",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak2.js:78:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #2",
                    "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:92:16)"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #2",
                    "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #2",
                    "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak2.js:112:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #3",
                    "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:131:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #3",
                    "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 2: expected 4 to equal 2",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "2",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 2: expected 4 to equal 2\n    at Context.<anonymous> (zadatak2.js:139:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #3",
                    "fullTitle": "Tabela crtaj3() Broj vidljivih celija 6 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:153:16)"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #3",
                    "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
                      "showDiff": true,
                      "actual": "10",
                      "expected": "6",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak2.js:166:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #4",
                    "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #4",
                    "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #4",
                    "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:210:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #4",
                    "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:224:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #4",
                    "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ],
                "pending": [],
                "failures": [
                  {
                    "title": "Tabela nema redova #2",
                    "fullTitle": "Tabela crtaj2() Tabela nema redova #2",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima 1 red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima 1 red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:70:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #2",
                    "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 3: expected 4 to equal 3",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak2.js:78:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #2",
                    "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:92:16)"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #2",
                    "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak2.js:112:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #3",
                    "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:131:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #3",
                    "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 2: expected 4 to equal 2",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "2",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 2: expected 4 to equal 2\n    at Context.<anonymous> (zadatak2.js:139:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #3",
                    "fullTitle": "Tabela crtaj3() Broj vidljivih celija 6 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:153:16)"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #3",
                    "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
                      "showDiff": true,
                      "actual": "10",
                      "expected": "6",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak2.js:166:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #4",
                    "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:210:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #4",
                    "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:224:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  }
                ],
                "passes": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #1",
                    "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #1",
                    "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #2",
                    "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Tabela nema redova #4",
                    "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #4",
                    "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #4",
                    "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ]
              }
                `
            let rezultat1 = json_string_test;
            let rezultat2 = json_string_test;
            let stvarne_greske = ["Tabela crtaj2() Tabela nema redova #2"
            ,"Tabela crtaj2() Broj redova 4 #2"
            ,"Tabela crtaj2() Broj vidljivih celija 6 #2"
            ,"Tabela crtaj2() Vide se samo celije ispod dijagonale #2"
            ,"Tabela crtaj3() Tabela nema redova #3"
            ,"Tabela crtaj3() Broj redova 4 #3"
            ,"Tabela crtaj3() Broj vidljivih celija 6 #3"
            ,"Tabela crtaj3() Broj nevidljivih celija 10 #3"
            ,"Tabela crtaj4() Broj vidljivih celija 6 #4"
            ,"Tabela crtaj4() Broj nevidljivih celija 10 #4"]
            let rezultatParsiranje = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(rezultatParsiranje.promjena, "47%", "Promjena treba biti 50%");
            assert.deepEqual(rezultatParsiranje.greske, stvarne_greske, "Greske nisu iste");
        })
        it('Razliciti ulazni rezultati ali isti testovi', function(){
            let json_string_test_isti_testovi_1 = `{
                "stats": {
                  "suites": 2,
                  "tests": 3,
                  "passes": 1,
                  "pending": 0,
                  "failures": 2,
                  "start": "2021-11-18T16:23:32.695Z",
                  "end": "2021-11-18T16:23:32.701Z",
                  "duration": 6
                },
                "tests": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela nema redova: expected 0 to equal 3",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (test.js:9:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 6: expected 6 to equal 5",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "5",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 6: expected 6 to equal 5\n    at Context.<anonymous> (test.js:30:16)"
                    }
                  }
                ],
                "pending": [],
                "failures": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela nema redova: expected 0 to equal 3",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (test.js:9:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 6: expected 6 to equal 5",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "5",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 6: expected 6 to equal 5\n    at Context.<anonymous> (test.js:30:16)"
                    }
                  }
                ],
                "passes": [
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ]
              }`;
            let json_string_test_isti_testovi_2 = `{
                "stats": {
                  "suites": 2,
                  "tests": 3,
                  "passes": 2,
                  "pending": 0,
                  "failures": 1,
                  "start": "2021-11-18T16:25:10.616Z",
                  "end": "2021-11-18T16:25:10.622Z",
                  "duration": 6
                },
                "tests": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela nema redova: expected 0 to equal 3",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (test.js:9:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ],
                "pending": [],
                "failures": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela nema redova: expected 0 to equal 3",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (test.js:9:16)"
                    }
                  }
                ],
                "passes": [
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ]
              }`;
            let rezultat1 = json_string_test_isti_testovi_1;
            let rezultat2 = json_string_test_isti_testovi_2;
            let stvarne_greske = ["Tabela crtaj1() Tabela nema redova #1"]
            let rezultatParsiranje = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(rezultatParsiranje.promjena, "66%", "Promjena treba biti 50%");
            assert.deepEqual(rezultatParsiranje.greske, stvarne_greske, "Greske nisu iste");
        })

        //ODMAKNUTI BROJEVI na kraju svakog stringa u stvarne_greske govore iz kojeg rezultata je taj string
        it('Razliciti testovi', function(){
            let json_string_test = `{
                "stats": {
                  "suites": 5,
                  "tests": 19,
                  "passes": 9,
                  "pending": 0,
                  "failures": 10,
                  "start": "2021-11-17T18:40:46.180Z",
                  "end": "2021-11-17T18:40:46.189Z",
                  "duration": 9
                },
                "tests": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #1",
                    "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #1",
                    "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Tabela nema redova #2",
                    "fullTitle": "Tabela crtaj2() Tabela nema redova #2",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima 1 red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima 1 red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:70:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #2",
                    "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 3: expected 4 to equal 3",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak2.js:78:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #2",
                    "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:92:16)"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #2",
                    "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #2",
                    "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak2.js:112:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #3",
                    "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:131:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #3",
                    "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 2: expected 4 to equal 2",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "2",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 2: expected 4 to equal 2\n    at Context.<anonymous> (zadatak2.js:139:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #3",
                    "fullTitle": "Tabela crtaj3() Broj vidljivih celija 6 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:153:16)"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #3",
                    "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
                      "showDiff": true,
                      "actual": "10",
                      "expected": "6",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak2.js:166:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #4",
                    "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #4",
                    "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #4",
                    "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:210:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #4",
                    "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:224:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #4",
                    "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ],
                "pending": [],
                "failures": [
                  {
                    "title": "Tabela nema redova #2",
                    "fullTitle": "Tabela crtaj2() Tabela nema redova #2",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima 1 red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima 1 red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:70:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #2",
                    "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 3: expected 4 to equal 3",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak2.js:78:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #2",
                    "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:92:16)"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #2",
                    "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak2.js:112:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #3",
                    "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak2.js:131:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #3",
                    "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 2: expected 4 to equal 2",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "2",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 2: expected 4 to equal 2\n    at Context.<anonymous> (zadatak2.js:139:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #3",
                    "fullTitle": "Tabela crtaj3() Broj vidljivih celija 6 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak2.js:153:16)"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #3",
                    "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
                      "showDiff": true,
                      "actual": "10",
                      "expected": "6",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak2.js:166:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #4",
                    "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:210:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #4",
                    "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak2.js:224:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  }
                ],
                "passes": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #1",
                    "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #1",
                    "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #2",
                    "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Tabela nema redova #4",
                    "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #4",
                    "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #4",
                    "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ]
              }
                `
            let json_string_test_2 = `{
                "stats": {
                  "suites": 5,
                  "tests": 18,
                  "passes": 10,
                  "pending": 0,
                  "failures": 8,
                  "start": "2021-11-17T18:43:17.331Z",
                  "end": "2021-11-17T18:43:17.340Z",
                  "duration": 9
                },
                "tests": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #1",
                    "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #1",
                    "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #2",
                    "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 3: expected 4 to equal 3",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak3pomocni.js:78:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #2",
                    "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak3pomocni.js:92:16)"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #2",
                    "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #2",
                    "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:112:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #3",
                    "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak3pomocni.js:131:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #3",
                    "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #3",
                    "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
                      "showDiff": true,
                      "actual": "10",
                      "expected": "6",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak3pomocni.js:166:16)"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #3",
                    "fullTitle": "Tabela crtaj3() Vide se samo celije ispod dijagonale #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:173:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #4",
                    "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #4",
                    "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #4",
                    "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:210:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #4",
                    "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:224:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #4",
                    "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ],
                "pending": [],
                "failures": [
                  {
                    "title": "Broj redova 4 #2",
                    "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 3: expected 4 to equal 3",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (zadatak3pomocni.js:78:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #2",
                    "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (zadatak3pomocni.js:92:16)"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #2",
                    "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:112:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #3",
                    "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (zadatak3pomocni.js:131:16)"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #3",
                    "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
                      "showDiff": true,
                      "actual": "10",
                      "expected": "6",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (zadatak3pomocni.js:166:16)"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #3",
                    "fullTitle": "Tabela crtaj3() Vide se samo celije ispod dijagonale #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (zadatak3pomocni.js:173:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #4",
                    "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:210:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #4",
                    "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (zadatak3pomocni.js:224:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  }
                ],
                "passes": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #1",
                    "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #1",
                    "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #2",
                    "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #3",
                    "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Tabela nema redova #4",
                    "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #4",
                    "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #4",
                    "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ]
              }` 
            let rezultat1 = json_string_test;
            let rezultat2 = json_string_test_2;
            let stvarne_greske = ["Tabela crtaj2() Tabela nema redova #2"
                ,"Tabela crtaj3() Broj vidljivih celija 6 #3"
                ,"Tabela crtaj3() Vide se samo celije ispod dijagonale #3"]
            let rezultatParsiranje = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(rezultatParsiranje.promjena, "50%", "Promjena treba biti 50%");
            assert.deepEqual(rezultatParsiranje.greske, stvarne_greske, "Greske nisu iste");
        })
        it('Jedan rezultat bez testova', function(){
            let json_string_test_bez_testa = `{
                "stats": {
                  "suites": 0,
                  "tests": 0,
                  "passes": 0,
                  "pending": 0,
                  "failures": 0,
                  "start": "2021-11-18T17:21:16.622Z",
                  "end": "2021-11-18T17:21:16.623Z",
                  "duration": 1
                },
                "tests": [],
                "pending": [],
                "failures": [],
                "passes": []
              }`
            let json_string_test_tri_testa = `{
                "stats": {
                  "suites": 2,
                  "tests": 3,
                  "passes": 2,
                  "pending": 0,
                  "failures": 1,
                  "start": "2021-11-18T16:25:10.616Z",
                  "end": "2021-11-18T16:25:10.622Z",
                  "duration": 6
                },
                "tests": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela nema redova: expected 0 to equal 3",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (test.js:9:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ],
                "pending": [],
                "failures": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela nema redova: expected 0 to equal 3",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (test.js:9:16)"
                    }
                  }
                ],
                "passes": [
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ]
              }`;
            let rezultat1 = json_string_test_bez_testa;
            let rezultat2 = json_string_test_tri_testa; 
            let stvarne_greske = ["Tabela crtaj1() Tabela nema redova #1"]
            let rezultatParsiranje = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(rezultatParsiranje.promjena, "33%", "Promjena treba biti 50%");
            assert.deepEqual(rezultatParsiranje.greske, stvarne_greske, "Greske nisu iste");
        })
        it('Razliciti testovi #2', function(){
            let json_string_test_3 = `{
                "stats": {
                  "suites": 5,
                  "tests": 14,
                  "passes": 7,
                  "pending": 0,
                  "failures": 7,
                  "start": "2021-11-18T17:24:57.147Z",
                  "end": "2021-11-18T17:24:57.156Z",
                  "duration": 9
                },
                "tests": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela nema redova: expected 0 to equal 3",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (test.js:9:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #1",
                    "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #1",
                    "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #2",
                    "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 3: expected 4 to equal 3",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (test.js:77:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #2",
                    "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (test.js:91:16)"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #2",
                    "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #2",
                    "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (test.js:111:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #3",
                    "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (test.js:130:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #3",
                    "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #3",
                    "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
                      "showDiff": true,
                      "actual": "10",
                      "expected": "6",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (test.js:165:16)"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #3",
                    "fullTitle": "Tabela crtaj3() Vide se samo celije ispod dijagonale #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (test.js:172:16)"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #4",
                    "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ],
                "pending": [],
                "failures": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela nema redova: expected 0 to equal 3",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (test.js:9:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #2",
                    "fullTitle": "Tabela crtaj2() Broj redova 4 #2",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj redova treba biti 3: expected 4 to equal 3",
                      "showDiff": true,
                      "actual": "4",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj redova treba biti 3: expected 4 to equal 3\n    at Context.<anonymous> (test.js:77:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #2",
                    "fullTitle": "Tabela crtaj2() Broj vidljivih celija 6 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj vidljivih celija 10: expected 6 to equal 10",
                      "showDiff": true,
                      "actual": "6",
                      "expected": "10",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj vidljivih celija 10: expected 6 to equal 10\n    at Context.<anonymous> (test.js:91:16)"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #2",
                    "fullTitle": "Tabela crtaj2() Vide se samo celije ispod dijagonale #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (test.js:111:16)"
                    }
                  },
                  {
                    "title": "Tabela nema redova #3",
                    "fullTitle": "Tabela crtaj3() Tabela nema redova #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela ima red: expected 0 to equal 1",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "1",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela ima red: expected 0 to equal 1\n    at Context.<anonymous> (test.js:130:16)"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #3",
                    "fullTitle": "Tabela crtaj3() Broj nevidljivih celija 10 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Broj nevidljivih celija 6: expected 10 to equal 6",
                      "showDiff": true,
                      "actual": "10",
                      "expected": "6",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Broj nevidljivih celija 6: expected 10 to equal 6\n    at Context.<anonymous> (test.js:165:16)"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #3",
                    "fullTitle": "Tabela crtaj3() Vide se samo celije ispod dijagonale #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "message": "Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false",
                      "showDiff": true,
                      "actual": "true",
                      "expected": "false",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Celije trebaju biti samo ispod glavne dijagonale: expected true to equal false\n    at Context.<anonymous> (test.js:172:16)"
                    }
                  }
                ],
                "passes": [
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #1",
                    "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #1",
                    "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #2",
                    "fullTitle": "Tabela crtaj2() Broj nevidljivih celija 10 #2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #3",
                    "fullTitle": "Tabela crtaj3() Broj redova 4 #3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #4",
                    "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ]
              }`
            let json_string_test_4 = `{
                "stats": {
                  "suites": 3,
                  "tests": 10,
                  "passes": 7,
                  "pending": 0,
                  "failures": 3,
                  "start": "2021-11-18T17:26:50.089Z",
                  "end": "2021-11-18T17:26:50.097Z",
                  "duration": 8
                },
                "tests": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela nema redova: expected 0 to equal 3",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (test.js:9:16)"
                    }
                  },
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #1",
                    "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #1",
                    "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Tabela nema redova #4",
                    "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #4",
                    "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #4",
                    "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (test.js:209:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #4",
                    "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (test.js:223:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #4",
                    "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ],
                "pending": [],
                "failures": [
                  {
                    "title": "Tabela nema redova #1",
                    "fullTitle": "Tabela crtaj1() Tabela nema redova #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "err": {
                      "message": "Tabela nema redova: expected 0 to equal 3",
                      "showDiff": true,
                      "actual": "0",
                      "expected": "3",
                      "operator": "strictEqual",
                      "stack": "AssertionError: Tabela nema redova: expected 0 to equal 3\n    at Context.<anonymous> (test.js:9:16)"
                    }
                  },
                  {
                    "title": "Broj vidljivih celija 6 #4",
                    "fullTitle": "Tabela crtaj4() Broj vidljivih celija 6 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (test.js:209:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #4",
                    "fullTitle": "Tabela crtaj4() Broj nevidljivih celija 10 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "err": {
                      "stack": "ReferenceError: brojCelijaGRESKA is not defined\n    at Context.<anonymous> (test.js:223:17)",
                      "message": "brojCelijaGRESKA is not defined"
                    }
                  }
                ],
                "passes": [
                  {
                    "title": "Broj redova 4 #1",
                    "fullTitle": "Tabela crtaj1() Broj redova 4 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj vidljivih celija 6 #1",
                    "fullTitle": "Tabela crtaj1() Broj vidljivih celija 6 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj nevidljivih celija 10 #1",
                    "fullTitle": "Tabela crtaj1() Broj nevidljivih celija 10 #1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #1",
                    "fullTitle": "Tabela crtaj1() Vide se samo celije ispod dijagonale #1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Tabela nema redova #4",
                    "fullTitle": "Tabela crtaj4() Tabela nema redova #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Broj redova 4 #4",
                    "fullTitle": "Tabela crtaj4() Broj redova 4 #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  },
                  {
                    "title": "Vide se samo celije ispod dijagonale #4",
                    "fullTitle": "Tabela crtaj4() Vide se samo celije ispod dijagonale #4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                  }
                ]
              }`
            let rezultat1 = json_string_test_3;
            let rezultat2 = json_string_test_4;
            let stvarne_greske = ["Tabela crtaj2() Broj redova 4 #2"
            ,"Tabela crtaj2() Broj vidljivih celija 6 #2"  
            ,"Tabela crtaj2() Vide se samo celije ispod dijagonale #2"
            ,"Tabela crtaj3() Tabela nema redova #3" 
            ,"Tabela crtaj3() Broj nevidljivih celija 10 #3"  
            ,"Tabela crtaj3() Vide se samo celije ispod dijagonale #3"
            ,"Tabela crtaj4() Broj vidljivih celija 6 #4"
            ,"Tabela crtaj4() Broj nevidljivih celija 10 #4"]
            let rezultatParsiranje = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(rezultatParsiranje.promjena, "56%", "Promjena treba biti 50%");
            assert.deepEqual(rezultatParsiranje.greske, stvarne_greske, "Greske nisu iste");
        })
    })
})